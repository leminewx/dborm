package datatype

const (
	// 数值类型
	_MysqlBit       = "BIT"
	_MysqlTinyint   = "TINYINT"
	_MysqlSmallint  = "SMALLINT"
	_MysqlMediumint = "MEDIUMINT"
	_MysqlInt       = "INT" // 支持 AUTO_INCREMENT
	_MysqlInteger   = "INTEGER"
	_MysqlBigint    = "BIGINT" // 支持 AUTO_INCREMENT
	_MysqlReal      = "REAL"
	_MysqlFloat     = "FLOAT"
	_MysqlDouble    = "DOUBLE"
	_MysqlDecimal   = "DECIMAL"
	_MysqlNumeric   = "NUMERIC"

	// 字符类型
	_MysqlChar       = "CHAR"
	_MysqlVarchar    = "VARCHAR"
	_MysqlTinytext   = "TINYTEXT"
	_MysqlText       = "TEXT"
	_MysqlMediumtext = "MEDIUMTEXT"
	_MysqlLongtext   = "LONGTEXT"

	// 时间类型
	_MysqlDate      = "DATE"
	_MysqlDatetime  = "DATETIME"
	_MysqlTime      = "TIME"
	_MysqlTimestamp = "TIMESTAMP"

	// 字节类型
	_MysqlBinary     = "BINARY"
	_MysqlVarbinary  = "VARBINARY"
	_MysqlTinyblob   = "TINYBLOB"
	_MysqlBlob       = "BLOB"
	_MysqlMediumblob = "MEDIUMBLOB"
	_MysqlLongblob   = "LONGBLOB"
	_MysqlBytea      = "BLOB"

	// 其他类型
	_MysqlSet  = "SET"
	_MysqlJson = "JSON"
	_MysqlBool = "BOOL"
	_MysqlEnum = "ENUM"
)

var MysqlMap = map[string]string{
	Bit:       _MysqlBit,
	Tinyint:   _MysqlTinyint,
	Smallint:  _MysqlSmallint,
	Mediumint: _MysqlMediumint,
	Int:       _MysqlInt,
	Integer:   _MysqlInteger,
	Bigint:    _MysqlBigint,
	Real:      _MysqlReal,
	Float:     _MysqlFloat,
	Double:    _MysqlDouble,
	Decimal:   _MysqlDecimal,
	Numeric:   _MysqlNumeric,

	// 字符类型
	Char:       _MysqlChar,
	Varchar:    _MysqlVarchar,
	Tinytext:   _MysqlTinytext,
	Text:       _MysqlText,
	Mediumtext: _MysqlMediumtext,
	Longtext:   _MysqlLongtext,

	// 时间类型
	Date:       _MysqlDate,
	Datetime:   _MysqlDatetime,
	Time:       _MysqlTime,
	Timestamp:  _MysqlTimestamp,
	Timestampz: _MysqlText,

	// 字节类型
	Binary:     _MysqlBinary,
	Varbinary:  _MysqlVarbinary,
	Tinyblob:   _MysqlTinyblob,
	Blob:       _MysqlBlob,
	Mediumblob: _MysqlMediumblob,
	Longblob:   _MysqlLongblob,
	Bytea:      _MysqlBlob,

	// 其他类型
	Set:       _MysqlSet,
	Enum:      _MysqlEnum,
	Json:      _MysqlJson,
	Bool:      _MysqlBool,
	Serial:    _MysqlInt,
	Bigserial: _MysqlBigint,
}
