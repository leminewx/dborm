package datatype

import "testing"

func TestMysqlTypeMap(t *testing.T) {
	t.Log(MysqlMap[Varchar])
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkMysqlTypeMap-80    	65186320	        18.48 ns/op	       0 B/op	       0 allocs/op
func BenchmarkMysqlTypeMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if _, ok := MysqlMap[Bool]; !ok {
			b.Fatal("not found")
		}
	}
}
