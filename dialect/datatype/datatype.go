package datatype

/*
+---------------+---------------+-----------+-------------------+---------------------------+
|     dborm     |     mysql     |  sqlite3  |     postgresql    |           remark          |
+---------------+---------------+-----------+-------------------+---------------------------+
| BIT   		| BIT			| INTEGER	| BIT				|							|
| TINYINT		| TINYINT		| INTEGER	| SMALLINT			|							|
| SMALLINT		| SMALLINT		| INTEGER	| SMALLINT			|							|
| MEDIUMINT		| MEDIUMINT		| INTEGER	| INTEGER			|							|
| INT			| INT			| INTEGER	| INTEGER			|							|
| INTEGER		| INTEGER		| INTEGER	| INTEGER			|							|
| BIGINT		| BIGINT		| INTEGER	| BIGINT			|							|
+---------------+---------------+-----------+-------------------+---------------------------+
| CHAR			| CHAR			| TEXT		| CHAR				|							|
| VARCHAR		| VARCHAR		| TEXT		| VARCHAR			|							|
| TINYTEXT		| TINYTEXT		| TEXT		| TEXT				|							|
| TEXT			| TEXT			| TEXT		| TEXT				|							|
| MEDIUMTEXT	| MEDIUMTEXT	| TEXT		| TEXT				|							|
| LONGTEXT		| LONGTEXT		| TEXT		| TEXT				|							|
+---------------+---------------+-----------+-------------------+---------------------------+
| DATE			| DATE			| NUMERIC	| DATE				|							|
| DATETIME		| DATETIME		| NUMERIC	| TIMESTAMP			|							|
| TIME			| TIME			| NUMERIC	| TIME				|							|
| TIMESTAMP		| TIMESTAMP		| NUMERIC	| TIMESTAMP			|							|
| TIMESTAMPZ	| TEXT			| TEXT		| TIMESTAMP			| timestamp with zone info	|
+---------------+---------------+-----------+-------------------+---------------------------+
| REAL			| REAL			| REAL		| REAL				|							|
| FLOAT			| FLOAT			| REAL		| REAL				|							|
| DOUBLE		| DOUBLE		| REAL		| DOUBLE PRECISION	|							|
| DECIMAL		| DECIMAL		| NUMERIC	| DECIMAL			|							|
| NUMERIC		| NUMERIC		| NUMERIC	| NUMERIC			|							|
+---------------+---------------+-----------+-------------------+---------------------------+
| BINARY		| BINARY		| BLOB		| BYTEA				|							|
| VARBINARY		| VARBINARY		| BLOB		| BYTEA				|							|
| TINYBLOB		| TINYBLOB		| BLOB		| BYTEA				|							|
| BLOB			| BLOB			| BLOB		| BYTEA				|							|
| MEDIUMBLOB	| MEDIUMBLOB	| BLOB		| BYTEA				|							|
| LONGBLOB		| LONGBLOB		| BLOB		| BYTEA				|							|
| BYTEA			| BLOB			| BLOB		| BYTEA				|							|
+---------------+---------------+-----------+-------------------+---------------------------+
| BOOL			| TINYINT		| INTEGER	| BOOLEAN			|							|
| SERIAL		| INT			| INTEGER	| SERIAL			| auto increment			|
| BIGSERIAL		| BIGINT		| INTEGER	| BIGSERIAL			| auto increment			|
+---------------+---------------+-----------+-------------------+---------------------------+
*/

const (
	// 数值类型
	Bit       = "BIT"
	Tinyint   = "TINYINT"
	Smallint  = "SMALLINT"
	Mediumint = "MEDIUMINT"
	Int       = "INT"
	Integer   = "INTEGER"
	Bigint    = "BIGINT"
	Real      = "REAL"
	Float     = "FLOAT"
	Double    = "DOUBLE"
	Decimal   = "DECIMAL"
	Numeric   = "NUMERIC"

	// 字符类型
	Char       = "CHAR"
	Varchar    = "VARCHAR"
	Tinytext   = "TINYTEXT"
	Text       = "TEXT"
	Mediumtext = "MEDIUMTEXT"
	Longtext   = "LONGTEXT"

	// 时间类型
	Date       = "DATE"
	Datetime   = "DATETIME"
	Time       = "TIME"
	Timestamp  = "TIMESTAMP"
	Timestampz = "TIMESTAMPZ"

	// 字节类型
	Binary     = "BINARY"
	Varbinary  = "VARBINARY"
	Tinyblob   = "TINYBLOB"
	Blob       = "BLOB"
	Mediumblob = "MEDIUMBLOB"
	Longblob   = "LONGBLOB"
	Bytea      = "BYTEA"

	// 其他类型
	Set       = "SET"
	Enum      = "ENUM"
	Json      = "JSON"
	Bool      = "BOOL"
	Serial    = "SERIAL"
	Bigserial = "BIGSERIAL"
)
