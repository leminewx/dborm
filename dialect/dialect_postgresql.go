package dialect

// import (
// 	"fmt"
// 	"reflect"
// 	"time"
// )

// var _ Dialect = (*postgresql)(nil)

// func init() {
// 	Register(DRIVER_NAME_POSTGRESQL, &sqlite3{})
// }

// type postgresql struct{}

// func (own *postgresql) GetDriverName() string {
// 	return DRIVER_NAME_POSTGRESQL
// }

// func (own *postgresql) GetDataTypeOf(typ reflect.Value) string {
// 	switch typ.Kind() {
// 	case reflect.Bool:
// 		return "BOOL"
// 	case reflect.Int16:
// 		return "INT2"
// 	case reflect.Int32:
// 		return "INT4"
// 	case reflect.Int64:
// 		return "INT8"
// 	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uintptr:
// 		return "INTEGER"
// 	case reflect.Int64, reflect.Uint64:
// 		return "BIGINT"
// 	case reflect.Float32, reflect.Float64:
// 		return "REAL"
// 	case reflect.String:
// 		return "TEXT"
// 	case reflect.Array, reflect.Slice:
// 		return "BLOB"
// 	case reflect.Struct:
// 		if _, ok := typ.Interface().(time.Time); ok {
// 			return "DATE"
// 		}
// 	}

// 	panic(fmt.Sprintf("invalid sqlite datatype: %s (%s)", typ.Type().Name(), typ.Kind()))
// }

// func (own *postgresql) GetTableExistSQL(_, tabName string) (string, []any) {
// 	return "SELECT name FROM sqlite_master WHERE type='table' and name=?;", []any{tabName}
// }
