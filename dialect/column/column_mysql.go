package column

import (
	"strings"

	"gitee.com/leminewx/dborm/dialect/datatype"
	"gitee.com/leminewx/gokit/pool"
)

var _ Column = &MysqlColumn{}

type MysqlColumn baseColumn

const (
	_KeyFK               = "FK"
	_KeyCD               = "CD" // 级联删除
	_KeyIndex            = "INDEX"
	_KeyUnique           = "UNIQUE"
	_KeyAutoInc          = "AUTO_INCREMENT"
	_KeyNotNull          = "NOT NULL"
	_keyComment          = "comment"
	_KeyComment          = "COMMENT"
	_KeyDefault          = "DEFAULT"
	_KeyUnsigned         = "UNSIGNED"
	_keyCurrentTimestamp = "CURRENT_TIMESTAMP"
)

func (own *MysqlColumn) GetName() [2]string {
	return own.Name
}

// Parse 解析 tag 标签为 MySQL 数据表字段
func (own *MysqlColumn) Parse(tag string) {
	if len(tag) == 0 {
		return
	}

	params := strings.Split(tag, " ")
	for _, param := range params {
		if param == "" {
			continue
		}

		if own.parse(param); own.IsIg {
			return
		}
	}
}

// parse 解析 orm tag 中的各个参数
func (own *MysqlColumn) parse(tag string) {
	size := len(tag)

	switch tag {
	case _TagIg: // 不导入
		own.IsIg = true
	case _TagPk: // 主键
		own.IsPK = true
	case _TagUnique: // 唯一
		own.IsUnique = true
	case _TagIndex: // 索引
		own.IsIndex = true
	case _TagAutoinc: // 自增
		own.IsAutoinc = true
	case _TagCreated: // 插入时，默认使用当前时间戳
		own.Default = _keyCurrentTimestamp
		own.IsDefault = true
		own.IsCreated = true
	case _TagUpdated: // 插入和更新时，默认使用当前时间戳
		own.Default = _keyCurrentTimestamp
		own.IsDefault = true
		own.IsUpdated = true
	case _TagNotNull: // 插入数据不能为 NULL
		own.IsNotNull = true
	default:
		// 解析字段名称
		if strings.HasPrefix(tag, "'") && strings.HasSuffix(tag, "'") {
			if size < 3 {
				return
			}

			own.Name[1] = tag[1 : size-1]
			return
		}

		// 解析其他关键字及其参数：keyword(argument)
		key, arg := ParseKeywordAndArgument(tag)
		switch key {
		case _KeyDefault: // 默认值：default('xxx')
			own.IsDefault = true
			own.Default = arg
		case _KeyUnique: // 组合唯一：unique(unique_name)
			own.IsUnique = true
			own.Unique = arg
		case _KeyIndex: // 组合索引：index(index_name)
			own.IsIndex = true
			own.Index = arg
		case _KeyFK: // 关联外键：fk(table_a(own_1))，fk(table_a(own_1),cd)
			own.IsFK = true
			items := strings.Split(arg, ",")
			switch len(items) {
			case 1:
				own.Reference = arg
			case 2:
				own.Reference = items[0]
				switch items[1] {
				case _TagOdc:
					own.IsCascadeDelete = true
				}
			}

		case _KeyComment: // 注释：comment('这是条注释，且不能有空格')
			own.Comment = arg
		default:

			// 尝试解析数据类型
			if typ, ok := datatype.MysqlMap[key]; ok {
				own.Type = typ
				own.Size = arg
				return
			}

			if key == _KeyUnsigned {
				own.Type += " "
				own.Type += _KeyUnsigned
				return
			}
		}
	}
}

func (own *MysqlColumn) WithNameAndType(name string, typ string) Column {
	if own.Name[1] == "" {
		own.Name[1] = name
	}

	if own.Type == "" {
		own.Type = typ
	}

	return own
}

func (own *MysqlColumn) AllowAutoIncrement() bool {
	return own.Type == datatype.Bigint || own.Type == datatype.Int
}

func (own *MysqlColumn) IsIgnore() bool {
	return own.IsIg
}

func (own *MysqlColumn) IsAutoInsert() bool {
	return own.IsCreated || own.IsUpdated || own.IsAutoinc
}

func (own *MysqlColumn) GenerateSQL() string {
	buf := pool.DefaultStringBuilderPool.Alloc()

	// 字段名
	buf.WriteByte('`')
	buf.WriteString(own.Name[1])
	buf.WriteString("` ")

	// 数据类型
	if own.Size != "" {
		if strings.ContainsRune(own.Type, ' ') {
			buf.WriteString(strings.Replace(own.Type, " ", "("+own.Size+") ", 1))
		} else {
			buf.WriteString(own.Type)
			buf.WriteByte('(')
			buf.WriteString(own.Size)
			buf.WriteByte(')')
		}
	} else {
		buf.WriteString(own.Type)
	}
	buf.WriteByte(' ')

	// 索引
	if own.IsIndex && own.Index == "" {
		buf.WriteString("INDEX ")
	}

	// 值唯一
	if own.IsUnique && own.Unique == "" {
		buf.WriteString("UNIQUE ")
	}

	// 非空
	if own.IsNotNull {
		buf.WriteString("NOT NULL ")
	}

	// 默认值
	if own.IsDefault {
		buf.WriteString("DEFAULT ")
		buf.WriteString(own.Default)
		buf.WriteByte(' ')

		if own.IsUpdated && (own.Type == datatype.Timestamp || own.Type == datatype.Datetime) {
			buf.WriteString("ON UPDATE ")
			buf.WriteString(_keyCurrentTimestamp)
			buf.WriteByte(' ')
		}
	}

	// 自增
	if own.IsAutoinc && own.AllowAutoIncrement() {
		buf.WriteString("AUTO_INCREMENT ")
	}

	// 标注
	if own.Comment != "" {
		buf.WriteString("COMMENT ")
		buf.WriteString(own.Comment)
		buf.WriteByte(' ')
	}

	sql := buf.String()
	pool.DefaultStringBuilderPool.Free(buf)
	return sql
}

func (own *MysqlColumn) GetBaseColumn() *baseColumn {
	return (*baseColumn)(own)
}

/*
1.使用unique：
- 方式一：
	name VARCHAR(50) UNIQUE
- 方式二：
	name VARCHAR(50),
	pass VARCHAR(255),
	CONSTRAINT user_unique UNIQUE (name, pass)
*/
