package column

import (
	"fmt"
	"testing"
)

func generateTestTags() []string {
	return []string{
		"'id' tinyint pk autoinc comment('ID')",
		"'id' bigint pk autoinc comment('ID')",
		"'id' serial pk autoinc comment('ID')",
		"'id' bigserial pk autoinc comment('ID')",
		"'name' varchar(30) default('') comment('用户名')",
		"'age' smallint default(0) comment('年龄')",
		"'sex' bool notnull comment('性别：0-女；1-男')",
		"'email' varchar(255) unique default('') comment('邮箱地址')",
		"'registed_at' timestamp created comment('注册时间')",
		"'logined_at' datetime updated comment('登录时间')",
		"'group_id' integer(8) unsigned unique fk(group(id),odc) comment('所属组')",
	}
}

func TestMysqlColumn_Parse(t *testing.T) {
	tags := generateTestTags()
	for _, tag := range tags {
		col := &MysqlColumn{}
		col.Parse(tag)
		t.Log(col)
	}
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkColumn_New-80    	 2423395	       493.9 ns/op	     168 B/op	       9 allocs/op
func BenchmarkMysqlColumn_Parse(b *testing.B) {
	col := &MysqlColumn{}
	tag := "'id' bigint(8) unsigned pk autoinc comment('ID')"
	for i := 0; i < b.N; i++ {
		col.Parse(tag)
	}
}

func TestMysqlColumn_GenerateSQL(t *testing.T) {
	tags := generateTestTags()
	for _, tag := range tags {
		col := &MysqlColumn{}
		col.Parse(tag)

		fmt.Println(col.GenerateSQL())
	}
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkColumn_GenerateSQL-80    	 3613682	       336.4 ns/op	     200 B/op	       5 allocs/op
func BenchmarkMysqlColumn_GenerateSQL(b *testing.B) {
	col := &MysqlColumn{}
	col.Parse("'id' bigint(8) unsigned pk autoinc comment('ID')")

	for i := 0; i < b.N; i++ {
		col.GenerateSQL()
	}
}
