package column

import "testing"

func TestConvertToUpper(t *testing.T) {
	t.Log(ConvertToUpper([]byte("tinyint")))
	t.Log(ConvertToUpper([]byte("varchar(30)")))
	t.Log(ConvertToUpper([]byte("default('')")))
	t.Log(ConvertToUpper([]byte("comment('username')")))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// Benchmark_ConvertToUpper-80    	19337529	        63.87 ns/op	      24 B/op	       1 allocs/op
func BenchmarkConvertToUpper(b *testing.B) {
	data := []byte("comment('username')")
	for i := 0; i < b.N; i++ {
		ConvertToUpper(data)
	}
}

func TestConvertToLower(t *testing.T) {
	t.Log(ConvertToLower([]byte("TINYINT")))
	t.Log(ConvertToLower([]byte("VARCHAR(30)")))
	t.Log(ConvertToLower([]byte("DEFAULT('')")))
	t.Log(ConvertToLower([]byte("COMMENT('username')")))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// Benchmark_ConvertToLower-80    	18461376	        63.94 ns/op	      24 B/op	       1 allocs/op
func BenchmarkConvertToLower(b *testing.B) {
	data := []byte("COMMENT('username')")
	for i := 0; i < b.N; i++ {
		ConvertToLower(data)
	}
}

func TestParseConvertHumpToUnderline(t *testing.T) {
	t.Log(ConvertHumpToUnderline([]byte("ID")))
	t.Log(ConvertHumpToUnderline([]byte("Name")))
	t.Log(ConvertHumpToUnderline([]byte("UserName")))
	t.Log(ConvertHumpToUnderline([]byte("WhatAreDoing")))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/schema
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkConvertHumpToUnderline-80    	21626247	        57.45 ns/op	      24 B/op	       1 allocs/op
func BenchmarkConvertHumpToUnderline(b *testing.B) {
	data := []byte("WhatAreDoing")
	for i := 0; i < b.N; i++ {
		ConvertHumpToUnderline(data)
	}
}

func TestParseKeywordAndArgument(t *testing.T) {
	t.Log(ParseKeywordAndArgument("tinyint"))
	t.Log(ParseKeywordAndArgument("bigint(4)"))
	t.Log(ParseKeywordAndArgument("varchar(30)"))
	t.Log(ParseKeywordAndArgument("default('')"))
	t.Log(ParseKeywordAndArgument("default(30)"))
	t.Log(ParseKeywordAndArgument("comment('username')"))
	t.Log(ParseKeywordAndArgument("comment('user name')"))
	t.Log(ParseKeywordAndArgument("fk(groups(id),cd)"))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// Benchmark_ParseKeywordAndArgument-80    	21044066	        54.81 ns/op	      16 B/op	       2 allocs/op
func BenchmarkParseKeywordAndArgument(b *testing.B) {
	data := "comment('user name')"
	for i := 0; i < b.N; i++ {
		ParseKeywordAndArgument(data)
	}
}
