package column

type (
	// Column 定义数据表字段的接口类型
	Column interface {
		// Parse 解析 tag 标签信息
		Parse(tag string)
		// GenerateSQL 生成创建数据表字段的 SQL 语句
		GenerateSQL() string
		// WithNameAndType 重置字段名称和类型
		WithNameAndType(name, typ string) Column
		// AllowAutoIncrement 字段类型是否允许自增
		AllowAutoIncrement() bool
		// IsIgnore 是否忽略该字段(不关联到数据库)
		IsIgnore() bool
		// IsAutoInsert 是否为自动插入自动
		IsAutoInsert() bool
		// GetName 获取字段名(0-数据模型字段名；1-数据表字段名)
		GetName() [2]string
		// GetBaseColumn 获取字段信息
		GetBaseColumn() *baseColumn
	}

	// Name 定义字段名称的类型（Name[0]-模型字段名称；Name[1]-数据表字段名称）
	Name [2]string
)

const (
	_TagIg      = "-"   // 不关联到数据库
	_TagPk      = "pk"  // primary key 主键
	_TagFk      = "fk"  // foreign key 外键
	_TagOdc     = "odc" // foreign key 外键级联删除
	_TagName    = "name"
	_TagIndex   = "index"
	_TagUnique  = "unique"
	_TagNotNull = "notnull"
	_TagAutoinc = "autoinc"
	_TagCreated = "created"
	_TagUpdated = "updated"
	_TagDeleted = "deleted"
	_TagDefault = "default"
	_TagComment = "comment"
)

// baseColumn 定义数据表字段的基础数据结构
type baseColumn struct {
	IsIg            bool // 是否不关联到数据库
	IsPK            bool // 是否为主键
	IsFK            bool // 是否关联外键
	IsIndex         bool // 是否索引
	IsUnique        bool // 是否唯一
	IsAutoinc       bool // 是否自增
	IsNotNull       bool // 是否非空
	IsDefault       bool // 是否有默认值
	IsCreated       bool // 是否自动插入当前时间戳
	IsUpdated       bool // 是否自动更新当前时间戳
	IsCascadeDelete bool // 外键是否级联删除

	Name      [2]string // 字段名
	Type      string    // 字段类型
	Size      string    // 类型长度
	Index     string    // 索引名称
	Unique    string    // unique名称
	Default   string    // 默认值
	Comment   string    // 注释
	Reference string    // 外键引用

}

func ParseKeywordAndArgument(val string) (string, string) {
	size := len(val)
	for i, v := range val {
		if v == '(' && val[size-1] == ')' {
			return ConvertToUpper([]byte(val[:i])), val[i+1 : size-1]
		}
	}

	return ConvertToUpper([]byte(val)), ""
}

const (
	UpperLetterStartIdx = 65
	LowerLetterStartIdx = 97
)

// ConvertHumpToUnderline 将驼峰英文命名转换为下划线英文命名
func ConvertHumpToUnderline(column []byte) string {
	buf := make([]byte, 0, len(column)*2)
	for i, r := range column {
		if UpperLetterStartIdx <= r && r < UpperLetterStartIdx+26 {
			if i > 0 {
				buf = append(buf, '_')
			}

			buf = append(buf, r+32)
		} else {
			buf = append(buf, r)
		}
	}

	return string(buf)
}

// ConvertToLower 将大写英文字母转为小写英文字母
func ConvertToLower(val []byte) string {
	buf := make([]byte, 0, len(val))
	for _, r := range val {
		if UpperLetterStartIdx <= r && r < UpperLetterStartIdx+26 {
			buf = append(buf, r+32)
		} else {
			buf = append(buf, r)
		}
	}

	return string(buf)
}

// ConvertToUpper 将小写英文字母转为大写英文字母
func ConvertToUpper(val []byte) string {
	buf := make([]byte, 0, len(val))
	for _, r := range val {
		if LowerLetterStartIdx <= r && r < LowerLetterStartIdx+26 {
			buf = append(buf, r-32)
		} else {
			buf = append(buf, r)
		}
	}

	return string(buf)
}
