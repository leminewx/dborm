package dialect

// var _ Dialect = (*sqlite3)(nil)

// func init() {
// 	Register(NameSqlite3, &sqlite3{})
// }

// type sqlite3 struct{}

// func (own *sqlite3) GetName() string {
// 	return NameSqlite3
// }

// func (own *sqlite3) GetType(typ string) (string, bool) {
// 	typ = strings.ToLower(strings.Split(typ, "(")[0])
// 	return "", typ != ""
// }

// func (own *sqlite3) ConvertDefaultType(value reflect.Value) string {
// 	switch value.Kind() {
// 	case reflect.Bool:
// 		return "BOOLEAN"
// 	case reflect.String:
// 		return "VARCHAR"
// 	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uintptr:
// 		return "INTEGER"
// 	case reflect.Int64, reflect.Uint64:
// 		return "BIGINT"
// 	case reflect.Float32:
// 		return "FLOAT"
// 	case reflect.Float64:
// 		return "DOUBLE"
// 	case reflect.Array, reflect.Slice:
// 		if _, ok := value.Interface().([]byte); ok {
// 			return "BLOB"
// 		}
// 	case reflect.Struct:
// 		if _, ok := value.Interface().(time.Time); ok {
// 			return "DATETIME"
// 		}
// 	}

// 	panic(fmt.Sprintf("invalid sqlite3 datatype: %s (%s)", value.Type().Name(), value.Kind()))
// }

// func (own *sqlite3) GetQueryingTableSQL(_, tabName string) (string, []any) {
// 	return "SELECT name FROM sqlite_master WHERE type='table' and name=?;", []any{tabName}
// }
