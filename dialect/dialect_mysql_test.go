package dialect

import (
	"fmt"
	"testing"

	"gitee.com/leminewx/dborm/dialect/column"
)

func TestMysql_FindSameValues(t *testing.T) {
	t.Log(findSameValues(
		map[string]struct{}{"a": {}, "b": {}, "d": {}},
		map[string]struct{}{"b": {}, "c": {}, "d": {}},
	))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkMysql_FindSameValues-80    	 3970728	       300.8 ns/op	     128 B/op	       3 allocs/op
func BenchmarkMysql_FindSameValues(b *testing.B) {
	mapa := map[string]struct{}{"a": {}, "b": {}, "d": {}}
	mapb := map[string]struct{}{"b": {}, "c": {}, "d": {}}
	for i := 0; i < b.N; i++ {
		findSameValues(mapa, mapb)
	}
}

func TestMysql_ExtractUniqueAndIndexWithTheSameName(t *testing.T) {
	indexs := map[string]map[string]struct{}{
		"a": {"c1": {}, "c3": {}},
		"b": {"c5": {}, "c7": {}},
		"c": {"c9": {}},
		"d": {"c2": {}},
	}

	uniques := map[string]map[string]struct{}{
		"a": {"c1": {}, "c5": {}},
		"b": {"c3": {}, "c7": {}},
		"c": {"c9": {}},
		"e": {"c4": {}},
	}

	nuniques, nindexs, uniqueIdxs := extractUniqueAndIndexWithTheSameName(uniques, indexs)
	t.Log("uniques: ", nuniques)
	t.Log("indexs: ", nindexs)
	t.Log("unique indexs: ", uniqueIdxs)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkMysql_FindSameValues-80    	 3970728	       300.8 ns/op	     128 B/op	       3 allocs/op
func BenchmarkMysql_ExtractUniqueAndIndexWithTheSameName(b *testing.B) {
	indexs := map[string]map[string]struct{}{
		"a": {"c1": {}, "c3": {}},
		"b": {"c5": {}, "c7": {}},
		"c": {"c9": {}},
		"d": {"c2": {}},
	}

	uniques := map[string]map[string]struct{}{
		"a": {"c1": {}, "c5": {}},
		"b": {"c3": {}, "c7": {}},
		"c": {"c9": {}},
		"e": {"c4": {}},
	}

	for i := 0; i < b.N; i++ {
		extractUniqueAndIndexWithTheSameName(uniques, indexs)
	}
}

func generateTestColumns() []column.Column {
	colums := make([]column.Column, 0)
	for _, tag := range []string{
		"'id' bigint(8) unsigned pk autoinc comment('ID')",
		"'name' varchar(30) unique(idt) default('') comment('用户名')",
		"'age' tinyint unique(idt) default(0) comment('年龄')",
		"'sex' enum('boy','gril') notnull index comment('性别：0-女；1-男')",
		"'email' varchar(255) unique default('') comment('邮箱地址')",
		"'registed_at' timestamp created comment('注册时间')",
		"'logined_at' datetime updated comment('登录时间')",
		"'joined_id' varchar(255) fk(group(id),odc)",
	} {
		col := &column.MysqlColumn{}
		col.Parse(tag)
		colums = append(colums, col)
	}

	return colums
}

func TestMysql_GenerateCreatingTableSQL(t *testing.T) {
	dial := Get(Mysql)
	fmt.Println(dial.GenerateCreatingTableSQL("user", generateTestColumns()))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/dialect
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkMysqlTypeMap-80    	  129829	      8929 ns/op	    6987 B/op	     117 allocs/op
func BenchmarkMysql_GenerateCreatingTableSQL(b *testing.B) {
	dial := Get(Mysql)
	columns := generateTestColumns()
	for i := 0; i < b.N; i++ {
		dial.GenerateCreatingTableSQL("user", columns)
	}
}
