package dialect

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	"gitee.com/leminewx/dborm/dialect/column"
	"gitee.com/leminewx/dborm/dialect/datatype"
	"gitee.com/leminewx/gokit/pool"
)

var _ Dialect = (*mysql)(nil)

func init() {
	Register(Mysql, &mysql{})
}

type mysql struct{}

func (own *mysql) GetName() string {
	return Mysql
}

func (own *mysql) GetType(typ string) (string, bool) {
	var ok bool
	typ, ok = datatype.MysqlMap[typ]
	return typ, ok
}

func (own *mysql) NewColumn() column.Column {
	return &column.MysqlColumn{}
}

const (
	_TypeSize255  = "(255)"
	_TypeUnsigned = " UNSIGNED"
)

func (own *mysql) ConvertDefaultType(value reflect.Value) string {
	switch value.Kind() {
	case reflect.Bool:
		return datatype.MysqlMap[datatype.Bool]
	case reflect.String:
		return datatype.MysqlMap[datatype.Varchar] + _TypeSize255
	case reflect.Int8:
		return datatype.MysqlMap[datatype.Tinyint]
	case reflect.Int16:
		return datatype.MysqlMap[datatype.Smallint]
	case reflect.Int32:
		return datatype.MysqlMap[datatype.Integer]
	case reflect.Int64, reflect.Int:
		if _, ok := value.Interface().(time.Duration); ok {
			return datatype.MysqlMap[datatype.Time]
		}

		return datatype.MysqlMap[datatype.Bigint]
	case reflect.Uint8:
		return datatype.MysqlMap[datatype.Tinyint] + _TypeUnsigned
	case reflect.Uint16:
		return datatype.MysqlMap[datatype.Smallint] + _TypeUnsigned
	case reflect.Uint32:
		return datatype.MysqlMap[datatype.Integer] + _TypeUnsigned
	case reflect.Uint64, reflect.Uint:
		return datatype.MysqlMap[datatype.Bigint] + _TypeUnsigned
	case reflect.Float32:
		return datatype.MysqlMap[datatype.Float]
	case reflect.Float64:
		return datatype.MysqlMap[datatype.Double]
	case reflect.Map:
		return datatype.MysqlMap[datatype.Json]
	case reflect.Array, reflect.Slice:
		return datatype.MysqlMap[datatype.Blob]
	case reflect.Struct:
		if _, ok := value.Interface().(time.Time); ok {
			return datatype.MysqlMap[datatype.Datetime]
		}

		return datatype.MysqlMap[datatype.Json]
	}

	panic(fmt.Sprintf("dborm: invalid MySQL datatype: %s (%s)", value.Type().Name(), value.Kind()))
}

func (own *mysql) GenerateCreatingTableSQL(tabName string, columns []column.Column, engName ...string) string {
	buf := pool.DefaultStringBuilderPool.Alloc()
	buf.WriteString("CREATE TABLE ")
	buf.WriteString(tabName)
	buf.WriteString(" (\n\t")

	size := len(columns)
	pks := make([]string, 0, size)
	fks := make([][3]any, 0, size)
	indexs := make(map[string]map[string]struct{}, size)
	uniques := make(map[string]map[string]struct{}, size)
	for i, col := range columns {
		if i > 0 {
			buf.WriteString(",\n\t")
		}

		// 生成单个字段的 SQL 语句
		buf.WriteString(col.GenerateSQL())

		column := col.GetBaseColumn()

		if column.IsPK {
			pks = append(pks, column.Name[1])
		}

		if column.IsFK {
			fks = append(fks, [3]any{column.Name[1], column.Reference, column.IsCascadeDelete})
		}

		if column.IsIndex && column.Index != "" {
			index, ok := indexs[column.Index]
			if !ok {
				index = make(map[string]struct{}, size)
			}

			index[column.Name[1]] = struct{}{}
			indexs[column.Index] = index
		}

		if column.IsUnique && column.Unique != "" {
			unique, ok := uniques[column.Unique]
			if !ok {
				unique = make(map[string]struct{}, size)
			}

			unique[column.Name[1]] = struct{}{}
			uniques[column.Unique] = unique
		}
	}

	// 生成主键 SQL
	if len(pks) > 0 {
		buf.WriteString(",\n\tPRIMARY KEY (")
		buf.WriteString(strings.Join(pks, ", "))
		buf.WriteString(")")
	}

	// 生成外键 SQL
	for _, fk := range fks {
		buf.WriteString(",\n\tFOREIGN KEY (")
		buf.WriteString(fk[0].(string))
		buf.WriteString(") REFERENCES ")
		buf.WriteString(fk[1].(string))
		if fk[2].(bool) {
			buf.WriteString(" ON DELETE CASCADE")
		}
	}

	// 过滤 unique 和 index 的组合
	// 标签：
	// c1 unique(a) index(a)
	// c2 unique(b) index(c)
	// c3 unique(b)
	// c4 index(c)
	// 生成：
	// UNIQUE INDEX a (c1)
	// CONSTRAINT b UNIQUE (c2, c3)
	// INDEX c (c2, c3)
	newUniques, newIndexs, uniqueIndexs := extractUniqueAndIndexWithTheSameName(uniques, indexs)

	// 生成组合不唯一索引
	for key, index := range newIndexs {
		buf.WriteString(",\n\tINDEX ")
		buf.WriteString(key)
		buf.WriteString(" (")
		buf.WriteString(strings.Join(index, ", "))
		buf.WriteString(")")
	}

	// 生成组合唯一索引
	for key, index := range uniqueIndexs {
		buf.WriteString(",\n\tUNIQUE INDEX ")
		buf.WriteString(key)
		buf.WriteString(" (")
		buf.WriteString(strings.Join(index, ", "))
		buf.WriteString(")")
	}

	// 生成组合唯一
	// CONSTRAINT b UNIQUE (c2, c3)
	for key, unique := range newUniques {
		buf.WriteString(",\n\tCONSTRAINT ")
		buf.WriteString(key)
		buf.WriteString(" UNIQUE (")
		buf.WriteString(strings.Join(unique, ", "))
		buf.WriteString(")")
	}

	buf.WriteString("\n) ENGINE=")
	if len(engName) == 0 {
		buf.WriteString("InnoDB")
	} else {
		buf.WriteString(engName[0])
	}
	buf.WriteString(" DEFAULT CHARSET=utf8;")

	sql := buf.String()
	pool.DefaultStringBuilderPool.Free(buf)
	return sql
}

func extractUniqueAndIndexWithTheSameName(uniques, indexs map[string]map[string]struct{}) (newUniques, newIndexs, uniqueIndexs map[string][]string) {
	newUniques = make(map[string][]string, len(uniques))
	newIndexs = make(map[string][]string, len(indexs))
	uniqueIndexs = make(map[string][]string, len(indexs)+len(uniques))

	for key, unique := range uniques {
		index, ok := indexs[key]
		if !ok {
			newUniques[key] = convertMapToSlice(unique)
			continue
		}

		nUniques, nIndexs, uniqueIdxs := findSameValues(unique, index)
		if len(nUniques) > 0 {
			newUniques[key] = nUniques
		}

		if len(nIndexs) > 0 {
			newIndexs[key] = nIndexs
		}

		if len(uniqueIdxs) > 0 {
			uniqueIndexs[key] = uniqueIdxs
		}
	}

	for key, index := range indexs {
		if _, ok := newIndexs[key]; !ok {
			_indexs := convertMapToSlice(index)
			if len(_indexs) > 0 {
				newIndexs[key] = _indexs
			}
		}
	}

	return
}

func findSameValues(aMap, bMap map[string]struct{}) (aSlice, bSlice, same []string) {
	aSlice = make([]string, 0, len(aMap))
	same = make([]string, 0, len(aMap)+len(bMap))

	for i := range aMap {
		if _, ok := bMap[i]; ok {
			same = append(same, i)
			delete(bMap, i)
			continue
		}

		aSlice = append(aSlice, i)
	}

	if len(bMap) > 0 {
		bSlice = convertMapToSlice(bMap)
	}

	return
}

func convertMapToSlice(data map[string]struct{}) []string {
	res := make([]string, 0, len(data))
	for val := range data {
		res = append(res, val)
	}

	return res
}

func (own *mysql) GenerateQueryingTableSQL(dbName, tabName string) string {
	return fmt.Sprintf("SELECT table_name FROM information_schema.tables WHERE table_schema='%s' AND table_name='%s';", dbName, tabName)
}
