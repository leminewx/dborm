package dialect

import (
	"reflect"

	"gitee.com/leminewx/dborm/dialect/column"
)

const (
	Mysql      = "mysql"
	Sqlite3    = "sqlite3"
	Postgresql = "postgresql"
)

type Dialect interface {
	// GetName 获取数据表的名称
	GetName() string
	// NewColumn 创建一个字段
	NewColumn() column.Column
	// GetType 判断指定类型是否存在
	GetType(typ string) (string, bool)
	// ConvertDefaultType 获取数据库默认类型
	ConvertDefaultType(val reflect.Value) string
	// GenerateCreatingColumnSQL 生成创建数据表的 SQL 语句
	GenerateCreatingTableSQL(tabName string, columns []column.Column, engName ...string) string
	// GenerateQueryingTableSQL 生成检查数据表是否存在的 SQL 语句
	GenerateQueryingTableSQL(dbName, tabName string) string
}

var dialectMap = map[string]Dialect{}

// Register 注册一个 Dialect
func Register(name string, dialect Dialect) {
	dialectMap[name] = dialect
}

// Get 获取指定驱动的 Dialect
func Get(name string) Dialect {
	return dialectMap[name]
}

func ParseKeywordAndArgument(val string) (string, string) {
	size := len(val)
	for i, v := range val {
		if v == '(' && val[size-1] == ')' {
			return ConvertToUpper([]byte(val[:i])), val[i+1 : size-1]
		}
	}

	return ConvertToUpper([]byte(val)), ""
}

const (
	UpperLetterStartIdx = 65
	LowerLetterStartIdx = 97
)

// ConvertHumpToUnderline 将驼峰英文命名转换为下划线英文命名
func ConvertHumpToUnderline(column []byte) string {
	buf := make([]byte, 0, len(column)*2)
	for i, r := range column {
		if UpperLetterStartIdx <= r && r < UpperLetterStartIdx+26 {
			if i > 0 {
				buf = append(buf, '_')
			}

			buf = append(buf, r+32)
		} else {
			buf = append(buf, r)
		}
	}

	return string(buf)
}

// ConvertToLower 将大写英文字母转为小写英文字母
func ConvertToLower(val []byte) string {
	buf := make([]byte, 0, len(val))
	for _, r := range val {
		if UpperLetterStartIdx <= r && r < UpperLetterStartIdx+26 {
			buf = append(buf, r+32)
		} else {
			buf = append(buf, r)
		}
	}

	return string(buf)
}

// ConvertToUpper 将小写英文字母转为大写英文字母
func ConvertToUpper(val []byte) string {
	buf := make([]byte, 0, len(val))
	for _, r := range val {
		if LowerLetterStartIdx <= r && r < LowerLetterStartIdx+26 {
			buf = append(buf, r-32)
		} else {
			buf = append(buf, r)
		}
	}

	return string(buf)
}
