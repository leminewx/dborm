package dborm

import (
	"testing"
	"time"

	"gitee.com/leminewx/dborm/statement/where"
	// _ "github.com/go-sql-driver/mysql"
)

type TestUser struct {
	Ignore     int64     `dborm:"-"`
	ID         int64     `dborm:"'id' pk autoinc comment('用户ID')"`
	Name       string    `dborm:"varchar(30) notnull index(idt) comment('用户名')"`
	Age        uint8     `dborm:"tinyint(1) default(0) comment('年龄')"`
	Sex        string    `dborm:"enum('boy','gril') notnull comment('0-男；1-女')"`
	Addr       string    `dborm:"default('') comment('地址')"`
	Email      string    `dborm:"unique index(idt) default('') comment('邮箱')"`
	RegistedAt time.Time `dborm:"created comment('注册时间')"`
	LoginedAt  time.Time `dborm:"timestamp updated comment('登录时间')"`
	GroupId    int       `dborm:"fk(test_groups(id)) comment('所属组ID')"`
	Extras     []byte
}

func (own *TestUser) GetTableName() string {
	return "test_users"
}

func TestEngine(t *testing.T) {
	engine, err := NewEngine(DriverMySQL, "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	if err != nil {
		t.Fatal(err)
	}
	defer engine.Close()

	sess := engine.NewSession(&TestUser{})

	// DELETE
	if num, err := sess.Where(where.EQ("name", "jjh")).Delete(); err != nil {
		t.Fatal(err)
	} else {
		t.Log(num)
	}

	// INSERT
	if num, err := sess.Insert(&TestUser{Name: "jjh", Age: 20, Sex: "gril", GroupId: 5}); err != nil {
		t.Fatal(err)
	} else {
		t.Log(num)
	}

	// UPDATE
	if num, err := sess.Where(where.EQ("name", "jjh")).Update(map[string]any{"addr": "chongqing"}); err != nil {
		t.Fatal(err)
	} else {
		t.Log(num)
	}

	// SELECT
	var users []TestUser
	if err := sess.Where(where.IN("group_id", 1, 3, 5)).Select(&users); err != nil {
		t.Fatal(err)
	}
	for _, user := range users {
		t.Log(user)
	}

	// COUNT
	if num, err := sess.Where(where.LT("age", 20)).Count(); err != nil {
		t.Fatal(err)
	} else {
		t.Log(num)
	}

	if num, err := sess.Where(where.GT("age", 20)).Count(); err != nil {
		t.Fatal(err)
	} else {
		t.Log(num)
	}

	if num, err := sess.Where(where.EQ("age", 20)).Count(); err != nil {
		t.Fatal(err)
	} else {
		t.Log(num)
	}
}

// func OpenDB(t *testing.T) *Engine {
// 	t.Helper()
// 	engine, err := NewEngine("sqlite3", "gee.db")
// 	if err != nil {
// 		t.Fatal("failed to connect", err)
// 	}
// 	return engine
// }

// func TestNewEngine(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// }

// type User struct {
// 	Name string `geeorm:"PRIMARY KEY"`
// 	Age  int
// }

// func transactionRollback(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// 	s := engine.NewSession()
// 	_ = s.Model(&User{}).DropTable()
// 	_, err := engine.Transaction(func(s *session.Session) (result interface{}, err error) {
// 		_ = s.Model(&User{}).CreateTable()
// 		_, err = s.Insert(&User{"Tom", 18})
// 		return nil, errors.New("Error")
// 	})
// 	if err == nil || s.HasTable() {
// 		t.Fatal("failed to rollback")
// 	}
// }

// func transactionCommit(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// 	s := engine.NewSession()
// 	_ = s.Model(&User{}).DropTable()
// 	_, err := engine.Transaction(func(s *session.Session) (result interface{}, err error) {
// 		_ = s.Model(&User{}).CreateTable()
// 		_, err = s.Insert(&User{"Tom", 18})
// 		return
// 	})
// 	u := &User{}
// 	_ = s.First(u)
// 	if err != nil || u.Name != "Tom" {
// 		t.Fatal("failed to commit")
// 	}
// }

// func TestEngine_Transaction(t *testing.T) {
// 	t.Run("rollback", func(t *testing.T) {
// 		transactionRollback(t)
// 	})
// 	t.Run("commit", func(t *testing.T) {
// 		transactionCommit(t)
// 	})
// }

// func TestEngine_Migrate(t *testing.T) {
// 	engine := OpenDB(t)
// 	defer engine.Close()
// 	s := engine.NewSession()
// 	_, _ = s.Raw("DROP TABLE IF EXISTS User;").Exec()
// 	_, _ = s.Raw("CREATE TABLE User(Name text PRIMARY KEY, XXX integer);").Exec()
// 	_, _ = s.Raw("INSERT INTO User(`Name`) values (?), (?)", "Tom", "Sam").Exec()
// 	engine.Migrate(&User{})

// 	rows, _ := s.Raw("SELECT * FROM User").QueryRows()
// 	columns, _ := rows.Columns()
// 	if !reflect.DeepEqual(columns, []string{"Name", "Age"}) {
// 		t.Fatal("Failed to migrate table User, got columns", columns)
// 	}
// }
