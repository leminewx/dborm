package schema

import (
	"fmt"
	"testing"
	"time"

	"gitee.com/leminewx/dborm/dialect"
)

type TestUser struct {
	Ignore     int       `dborm:"-"`
	ID         int64     `dborm:"'id' pk autoinc comment('用户ID')"`
	Name       string    `dborm:"varchar(30) notnull index(idt) comment('用户名')"`
	Age        uint8     `dborm:"tinyint(1) default(0) comment('年龄')"`
	Sex        bool      `dborm:"notnull comment('0-男；1-女')"`
	Email      string    `dborm:"unique(uemail) index(idt) default('') comment('邮箱')"`
	RegistedAt time.Time `dborm:"created comment('注册时间')"`
	LoginedAt  time.Time `dborm:"timestamp updated comment('登录时间')"`
	GroupId    int16     `dborm:"smallint(50) unsigned unique fk(group(id)) comment('所属组ID')"`
	Extras     []byte
}

func TestSchema_Parse(t *testing.T) {
	schema := Parse(dialect.Get(dialect.Mysql), &TestUser{})

	if column, ok := schema.GetColumn("id"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("name"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("age"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("sex"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("email"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("registed_at"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("logined_at"); ok {
		t.Log(column.GenerateSQL())
	}
	if column, ok := schema.GetColumn("group_id"); ok {
		t.Log(column.GenerateSQL())
	}
}

func TestSchema_GenerateCreatingTableSQL(t *testing.T) {
	dial := dialect.Get(dialect.Mysql)
	schema := Parse(dial, &TestUser{})

	fmt.Println(dial.GenerateCreatingTableSQL(schema.Name, schema.Columns, "MyISAM"))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/schema
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkSchema_Parse-80    	  141465	      8166 ns/op	    3712 B/op	     113 allocs/op
func BenchmarkSchema_Parse(b *testing.B) {
	dial := dialect.Get(dialect.Mysql)
	for i := 0; i < b.N; i++ {
		Parse(dial, &TestUser{})
	}
}

func TestSchema_RecordValues(t *testing.T) {
	dial := dialect.Get(dialect.Mysql)
	schema := Parse(dial, &TestUser{})
	t.Log(schema)

	values := schema.ExtractValues(TestUser{
		ID:      321,
		Name:    "wx",
		Age:     20,
		Sex:     false,
		Email:   "xxx@163.com",
		GroupId: 3,
	})
	t.Log(values)

	values = schema.ExtractValues(&TestUser{
		ID:      123,
		Name:    "lemine",
		Age:     18,
		Sex:     true,
		Email:   "xxx@qq.com",
		GroupId: 3,
	})
	t.Log(values)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/schema
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkSchema_RecordValues-80    	 1465826	       817.1 ns/op	     224 B/op	      11 allocs/op
func BenchmarkSchema_RecordValues(b *testing.B) {
	dial := dialect.Get(dialect.Mysql)
	schema := Parse(dial, &TestUser{})

	user := &TestUser{
		ID:      123,
		Name:    "lemine",
		Age:     18,
		Sex:     true,
		Email:   "xxx@qq.com",
		GroupId: 3,
	}
	for i := 0; i < b.N; i++ {
		schema.ExtractValues(user)
	}
}

// func BenchmarkSchema_Parse(b *testing.B) {
// 	type User struct {
// 		Name  string `orm:"type:varchar;size:50;pk:true" json:"name"`
// 		Sex   int8   `orm:"comment:0-girl,1-boy" json:"sex"`
// 		Age   int    `orm:"column:age" json:"age"`
// 		Addr  string `orm:"type:varchar;size:50" json:"addr"`
// 		Email string `orm:"column:email;type:varchar;size:100" json:"email"`
// 	}

// 	dial, _ := dialect.Get("mysql")
// 	for i := 0; i < b.N; i++ {
// 		Parse(&User{}, dial)
// 	}
// }

// func BenchmarkSchema_ExtractValues(b *testing.B) {
// 	type User struct {
// 		Name  string `orm:"type:varchar;size:50;pk:true" json:"name"`
// 		Sex   int8   `orm:"comment:0-girl,1-boy" json:"sex"`
// 		Age   int    `orm:"column:age" json:"age"`
// 		Addr  string `orm:"type:varchar;size:50" json:"addr"`
// 		Email string `orm:"column:email;type:varchar;size:100" json:"email"`
// 	}
// 	user := User{
// 		Name:  "wx",
// 		Sex:   1,
// 		Age:   31,
// 		Addr:  "chongqing",
// 		Email: "wx@cictci.com",
// 	}

// 	dial, _ := dialect.Get("mysql")
// 	schema := Parse(&User{}, dial)
// 	for i := 0; i < b.N; i++ {
// 		schema.ExtractValues(&user)
// 	}
// }

// func TestXxx(t *testing.T) {
// 	type User struct {
// 		Name  string `orm:"type:varchar;size:50;pk:true" json:"name"`
// 		Sex   int8   `orm:"comment:0-girl,1-boy" json:"sex"`
// 		Age   int    `orm:"column:age" json:"age"`
// 		Addr  string `orm:"type:varchar;size:50" json:"addr"`
// 		Email string `orm:"column:email;type:varchar;size:100" json:"email"`
// 	}

// 	t.Log(reflect.Indirect(reflect.ValueOf(&User{})).Type())
// 	t.Log(reflect.TypeOf(&User{}))
// }
