package schema

import (
	"go/ast"
	"reflect"

	"gitee.com/leminewx/dborm/dialect"
	"gitee.com/leminewx/dborm/dialect/column"
)

const (
	_Tag = "dborm"
)

type TableNameGetter interface {
	GetTableName() string
}

// Schema 定义数据表的数据结构
type Schema struct {
	Name            string          // 数据表名称
	Model           any             // 数据模型
	ModelType       reflect.Type    // 数据模型类型
	Columns         []column.Column // 数据表的字段列表
	ColumnNames     []any           // 数据表的字段名列表
	InsertableNames []any           // 可插入字段名
	colmunMap       map[string]column.Column
}

// Parse 解析数据模型，用于建立与数据库的数据映射
func Parse(dial dialect.Dialect, model any) *Schema {
	modelType := reflect.Indirect(reflect.ValueOf(model)).Type()
	columnNum := modelType.NumField()

	// 获取数据表名
	var table string
	if tab, ok := model.(TableNameGetter); ok {
		table = tab.GetTableName()
	} else {
		table = dialect.ConvertHumpToUnderline([]byte(modelType.Name()))
	}

	// 创建数据表
	schema := &Schema{
		Name:            table,
		Model:           model,
		ModelType:       reflect.TypeOf(model),
		Columns:         make([]column.Column, 0, columnNum),
		ColumnNames:     make([]any, 0, columnNum),
		InsertableNames: make([]any, 0, columnNum),
		colmunMap:       make(map[string]column.Column, columnNum),
	}

	if schema.ModelType.Kind() == reflect.Ptr {
		schema.ModelType = schema.ModelType.Elem()
	}

	// 遍历数据模型的所有字段
	for i := 0; i < columnNum; i++ {
		field := modelType.Field(i)
		// 过滤掉匿名和不可导出字段
		if !field.Anonymous && ast.IsExported(field.Name) {
			// 解析 tag 标签
			column := dial.NewColumn()
			column.Parse(field.Tag.Get(_Tag))
			if column.IsIgnore() {
				continue
			}

			// 处理字段信息
			cName := dialect.ConvertHumpToUnderline([]byte(field.Name))
			cType := dial.ConvertDefaultType(reflect.Indirect(reflect.New(field.Type)))
			column.WithNameAndType(cName, cType)
			column.GetBaseColumn().Name[0] = field.Name

			// 处理数据表信息
			schema.Columns = append(schema.Columns, column)
			schema.ColumnNames = append(schema.ColumnNames, column.GetName()[1])
			if !column.IsAutoInsert() {
				schema.InsertableNames = append(schema.InsertableNames, column.GetName()[1])
			}
			schema.colmunMap[column.GetName()[1]] = column
		}
	}

	return schema
}

// GetColumn 获取数据表中的指定字段
func (own *Schema) GetColumn(name string) (column.Column, bool) {
	colmun, ok := own.colmunMap[name]
	return colmun, ok
}

// ExtractValues 从数据模型中提取数据
func (own *Schema) ExtractValues(object any) []any {
	columnValues := make([]any, 0, len(own.Columns))
	destValue := reflect.Indirect(reflect.ValueOf(object))
	for _, column := range own.Columns {
		if column.IsAutoInsert() {
			continue
		}

		columnValues = append(columnValues, destValue.FieldByName(column.GetName()[0]).Interface())
	}

	return columnValues
}
