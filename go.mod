module gitee.com/leminewx/dborm

go 1.22.5

require (
	gitee.com/leminewx/gokit v0.0.0-20241104142220-a98f5d9bbef8
	gitee.com/leminewx/loggo v0.0.0-20241122055800-746aa539a268
)
