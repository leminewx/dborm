package statement

// Statement 定义 SQL 语句的数据结构
type Statement struct {
	table string
	sql   map[ClauseType][]byte
	vars  map[ClauseType][]any
}

// NewStatement 创建一个 SQL 语句
func NewStatement(table string) *Statement {
	return &Statement{table: table}
}

// WithTableName 指定数据表名
func (own *Statement) WithTableName(name string) *Statement {
	own.table = name
	return own
}

// WithClause 添加 SQL 子句
func (own *Statement) WithClause(typ ClauseType, vars ...any) *Statement {
	if own.sql == nil {
		own.sql = make(map[ClauseType][]byte)
		own.vars = make(map[ClauseType][]any)
	}

	own.sql[typ], own.vars[typ] = generators[typ](own.table, vars...)
	if typ == ClauseSelect {
		own.sql[ClauseFrom], _ = generators[ClauseFrom](own.table)
	}

	return own
}

// GenerateSQL 生成完整的 SQL 语句和参数列表
func (own *Statement) GenerateSQL(types ...ClauseType) (string, []any) {
	vars := make([]any, 0)
	sqls := make([]byte, 0, len(types))
	for _, typ := range types {
		if sql, ok := own.sql[typ]; ok {
			sqls = append(sqls, sql...)
			vars = append(vars, own.vars[typ]...)
		}
	}

	return string(sqls), vars
}

// Reset 重置 SQL 语句的子句和参数
func (own *Statement) Reset() {
	own.sql = nil
	own.vars = nil
}
