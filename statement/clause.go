package statement

import (
	"bytes"

	"gitee.com/leminewx/dborm/statement/where"
)

// ClauseType 定义 SQL 子句的数据类型
type ClauseType int

const (
	ClauseInsert            ClauseType = iota // insert
	ClauseReplaceMysql                        // replace(MySQL)
	ClauseReplacePostgresql                   // replace(PostgreSQL)
	ClauseUpdate                              // update
	ClauseDelete                              // delete
	ClauseSelect                              // select
	ClauseCount                               // count
	ClauseValues                              // values
	ClauseDistinct                            // distinct
	ClauseFrom                                // from
	ClauseInnerJoin                           // inner join
	ClauseLeftJoin                            // left join
	ClauseRightJoin                           // right join
	ClauseWhere                               // where
	ClauseGroupBy                             // group by
	ClauseHaving                              // having
	ClauseOrderBy                             // order by
	ClauseLimit                               // limit
)

const _SqlComma = ", "

var generators map[ClauseType]generator

func init() {
	generators = map[ClauseType]generator{
		ClauseInsert:       _insert,
		ClauseReplaceMysql: _replace_mysql,
		ClauseUpdate:       _update,
		ClauseDelete:       _delete,
		ClauseSelect:       _select,
		ClauseCount:        _count,
		ClauseValues:       _values,
		ClauseDistinct:     _distinct,
		ClauseFrom:         _from,
		// ClauseInnerJoin: _innerJoin,
		// ClauseLeftJoin: _leftJoin,
		// ClauseRightJoin: _rightJoin,
		ClauseWhere: _where,
		// ClauseHaving: _having,
		ClauseGroupBy: _groupBy,
		ClauseOrderBy: _orderBy,
		ClauseLimit:   _limit,
	}
}

// generator 生成 SQL 子句和参数
type generator func(table string, values ...any) ([]byte, []any)

func _insert(table string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "INSERT INTO "...)
	sql = append(sql, table...)

	if len(values) == 0 {
		sql = append(sql, ' ')
		return sql, []any{}
	}

	sql = append(sql, " ("...)
	sql = bindColumnNames(sql, values...)
	sql = append(sql, ") "...)
	return sql, []any{}
}

func _replace_mysql(table string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "REPLACE INTO "...)
	sql = append(sql, table...)

	if len(values) == 0 {
		sql = append(sql, ' ')
		return sql, []any{}
	}

	sql = append(sql, " ("...)
	sql = bindColumnNames(sql, values...)
	sql = append(sql, ") "...)
	return sql, []any{}
}

// func _replace_postgresql(table string, values ...any) ([]byte, []any) {
// 	// INSERT INTO users (id, name, email)
// 	// VALUES (1, 'Jane Doe', 'jane@example.com')
// 	// ON CONFLICT (id)
// 	// DO UPDATE SET
// 	// 	name = EXCLUDED.name,
// 	// 	email = EXCLUDED.email;
// 	return nil, nil
// }

func _update(table string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	if len(values) == 0 {
		return sql, []any{}
	}

	sql = append(sql, "UPDATE "...)
	sql = append(sql, table...)
	sql = append(sql, " SET "...)

	vars := make([]any, 0)
	if len(values) == 1 {
		params, ok := values[0].(map[string]any)
		if ok {
			first := true
			for key, val := range params {
				if !first {
					sql = append(sql, "=?, "...)
				}

				sql = append(sql, key...)
				vars = append(vars, val)
				first = false
			}

			if len(vars) > 0 {
				sql = append(sql, "=? "...)
			}

			return sql, vars
		}
	}

	for i := 0; i < len(values); i += 2 {
		val, ok := values[i].(string)
		if !ok || values[i+1] == nil {
			continue
		}

		if i > 0 {
			sql = append(sql, "=?, "...)
		}

		sql = append(sql, val...)
		vars = append(vars, values[i+1])
	}

	if len(vars) > 0 {
		sql = append(sql, "=? "...)
	}

	return sql, vars
}

func _delete(table string, _ ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "DELETE FROM "...)
	sql = append(sql, table...)
	sql = append(sql, ' ')
	return sql, []any{}
}

func _select(table string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "SELECT "...)

	if len(values) == 0 {
		sql = append(sql, '*')
	} else {
		sql = bindColumnNames(sql, values...)
	}
	sql = append(sql, ' ')

	return sql, []any{}
}

func _count(table string, _ ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "SELECT COUNT(*) FROM "...)
	sql = append(sql, table...)
	sql = append(sql, ' ')
	return sql, []any{}
}

func _values(_ string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "VALUES "...)

	vars := make([]any, 0)
	for i, value := range values {
		val, ok := value.([]any)
		if !ok || len(val) == 0 {
			continue
		}

		if i > 0 {
			sql = append(sql, _SqlComma...)
		}

		sql = append(sql, '(')
		sql = bindVariables(sql, len(val))
		sql = append(sql, ')')
		vars = append(vars, val...)
	}
	sql = append(sql, ' ')

	return sql, vars
}

func _distinct(_ string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	if len(values) > 0 {
		sql = append(sql, "DISTINCT "...)
		sql = bindColumnNames(sql, values...)
		sql = append(sql, ' ')
	}

	return sql, []any{}
}

func _from(table string, _ ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	sql = append(sql, "FROM "...)
	sql = append(sql, table...)
	sql = append(sql, ' ')
	return sql, []any{}
}

const (
	_wherePrefixOR  = "OR "
	_wherePrefixAND = "AND"
)

func _where(_ string, values ...any) ([]byte, []any) {
	if len(values) == 0 {
		return []byte{}, []any{}
	}

	sql := make([]byte, 0)
	sql = append(sql, "WHERE "...)
	vars := make([]any, 0)

	first := true
	for _, value := range values {
		where, ok := value.(*where.Where)
		if !ok || len(where.Dest) < 3 {
			continue
		}

		// 检查第一个条件是否带有连接符 AND/OR，若有则需要去掉
		if first {
			first = false
			switch string(where.Dest[:3]) {
			case _wherePrefixOR:
				sql = append(sql, where.Dest[3:]...)
			case _wherePrefixAND:
				sql = append(sql, where.Dest[4:]...)
			default:
				sql = append(sql, where.Dest...)
			}
			vars = append(vars, where.Args...)
			continue
		}

		// 若条件之间没有连接符，默认使用 AND
		key := string(where.Dest[:3])
		if key != _wherePrefixOR && key != _wherePrefixAND {
			sql = append(sql, "AND "...)
		}

		sql = append(sql, where.Dest...)
		vars = append(vars, where.Args...)
	}

	if first {
		return []byte{}, vars
	}

	return sql, vars
}

func _groupBy(_ string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	if len(values) == 0 {
		return sql, []any{}
	}

	sql = append(sql, "GROUP BY "...)
	sql = bindColumnNames(sql, values...)
	sql = append(sql, ' ')
	return sql, []any{}
}

const (
	_keyASC  = "asc"
	_KeyASC  = "ASC"
	_keyDESC = "desc"
	_KeyDESC = "DESC"
)

// ORDER BY a, b DESC, c ASC, d
func _orderBy(_ string, values ...any) ([]byte, []any) {
	sql := make([]byte, 0)
	if len(values) == 0 {
		return sql, []any{}
	}

	sql = append(sql, "ORDER BY "...)
	first := true
	field := false
	for _, value := range values {
		val, ok := value.(string)
		if !ok || val == "" {
			continue
		}

		// 顺序或逆序
		if val == _KeyASC || val == _keyASC || val == _KeyDESC || val == _keyDESC {
			if !field {
				continue
			}

			sql = append(sql, ' ')
			sql = append(sql, bytes.ToUpper([]byte(val))...)
			first = false
			field = false
			continue
		}

		// 已经写入了一个字段名
		if field {
			sql = append(sql, _SqlComma...)
			sql = append(sql, val...)
			continue
		}

		// 写入一个新的字段名
		if !first {
			sql = append(sql, _SqlComma...)
		}
		sql = append(sql, val...)
		field = true
	}

	sql = append(sql, ' ')
	return sql, []any{}
}

func _limit(_ string, values ...any) ([]byte, []any) {
	vars := make([]any, 0)
	for _, value := range values {
		if ok := IsInteger(value); !ok {
			continue
		}

		if vars = append(vars, value); len(vars) == 2 {
			break
		}
	}

	switch len(vars) {
	case 0:
		return []byte{}, vars
	case 1:
		return []byte("LIMIT ? "), vars
	default:
		return []byte("LIMIT ?, ? "), vars
	}
}

func IsInteger(val any) bool {
	switch val.(type) {
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		return true
	default:
		return false
	}
}

func bindVariables(buf []byte, num int) []byte {
	for i := 0; i < num; i++ {
		if i > 0 {
			buf = append(buf, _SqlComma...)
		}

		buf = append(buf, '?')
	}

	return buf
}

func bindColumnNames(buf []byte, columns ...any) []byte {
	for i, column := range columns {
		col, ok := column.(string)
		if !ok || col == "" {
			continue
		}

		if i > 0 {
			buf = append(buf, _SqlComma...)
		}

		buf = append(buf, col...)
	}

	return buf
}
