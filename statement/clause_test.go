package statement

import (
	"testing"

	"gitee.com/leminewx/dborm/statement/where"
)

func TestClause_Insert(t *testing.T) {
	table := "user"
	columns := []any{}
	sql, vars := _insert(table, columns...)
	t.Log(string(sql))
	t.Log(vars...)

	columns = []any{"id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id"}
	sql, vars = _insert(table, columns...)
	t.Log(string(sql))
	t.Log(vars...)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Insert-80    	 5049172	       234.5 ns/op	     240 B/op	       4 allocs/op
func BenchmarkClause_Insert(b *testing.B) {
	table := "user"
	columns := []any{"id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id"}
	for i := 0; i < b.N; i++ {
		_insert(table, columns...)
	}
}

func TestClause_ReplaceMysql(t *testing.T) {
	table := "user"
	columns := []any{}
	sql, vars := _replace_mysql(table, columns...)
	t.Log(string(sql))
	t.Log(vars...)

	columns = []any{"id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id"}
	sql, vars = _replace_mysql(table, columns...)
	t.Log(string(sql))
	t.Log(vars...)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Insert-80    	 5049172	       234.5 ns/op	     240 B/op	       4 allocs/op
func BenchmarkClause_Mysql(b *testing.B) {
	table := "user"
	columns := []any{"id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id"}
	for i := 0; i < b.N; i++ {
		_insert(table, columns...)
	}
}

func TestClause_Update(t *testing.T) {
	table := "user"
	columns := []any{"id", 1, "name", "wx", "sex", "boy", "age", 18, "email", "xxx@qq.com", "addr", "chongqing", "group_id", 3}

	sql, vars := _update(table, columns...)
	t.Log(string(sql))
	t.Log(vars...)

	sql, vars = _update(table, map[string]any{
		"id": 1, "name": "wx", "sex": "boy", "age": 18, "email": "xxx@qq.com", "addr": "chongqing", "group_id": 3,
	})
	t.Log(string(sql))
	t.Log(vars...)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Update-80    	 2253619	       542.9 ns/op	     488 B/op	       9 allocs/op
func BenchmarkClause_Update(b *testing.B) {
	table := "user"
	columns := []any{"id", 1, "name", "wx", "sex", "boy", "age", 18, "email", "xxx@qq.com", "addr", "chongqing", "group_id", 3}

	for i := 0; i < b.N; i++ {
		_update(table, columns...)
	}
}

func TestClause_Delete(t *testing.T) {
	table := "user"

	sql, vars := _delete(table)
	t.Log(string(sql))
	t.Log(vars...)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Delete-80    	36602905	        33.26 ns/op	      16 B/op	       1 allocs/op
func BenchmarkClause_Delete(b *testing.B) {
	table := "user"
	for i := 0; i < b.N; i++ {
		_delete(table)
	}
}

func TestClause_Select(t *testing.T) {
	sql, _ := _select("user")
	t.Log(string(sql))

	sql, _ = _select("user", "*")
	t.Log(string(sql))

	sql, _ = _select("user", "id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id")
	t.Log(string(sql))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Select-80    	 4408722	       271.0 ns/op	     248 B/op	       5 allocs/op
func BenchmarkClause_Select(b *testing.B) {
	table := "user"
	columns := []any{"id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id"}
	for i := 0; i < b.N; i++ {
		_select(table, columns...)
	}
}

func TestClause_Values(t *testing.T) {
	users := []any{
		[]any{1, "wx", 1, 30, "wx@qq.com", "chongqing", 3},
		[]any{2, "gh", 1, 25, "gh@qq.com", "jiangsu", 2},
		[]any{3, "dsc", 1, 25, "dsc@qq.com", "shanxi", 1},
	}

	sql, vars := _values("", users...)
	t.Log(string(sql))
	t.Log(vars...)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Values-80    	 1623900	       727.9 ns/op	    1032 B/op	       8 allocs/op
func BenchmarkClause_Values(b *testing.B) {
	users := []any{
		[]any{1, "wx", 1, 30, "wx@qq.com", "chongqing", 3},
		[]any{2, "gh", 1, 25, "gh@qq.com", "jiangsu", 2},
		[]any{3, "dsc", 1, 25, "dsc@qq.com", "shanxi", 1},
	}

	for i := 0; i < b.N; i++ {
		_values("", users...)
	}
}

func TestClause_Distinct(t *testing.T) {
	sqlSelect, valSelect := _select("user", "name", "sex", "age")
	sqlDistinct, valDistinct := _distinct("", "name", "age")
	t.Log(string(sqlSelect) + string(sqlDistinct))
	t.Log(append(valSelect, valDistinct...))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Distinct-80    	 5125230	       235.5 ns/op	     240 B/op	       4 allocs/op
func BenchmarkClause_Distinct(b *testing.B) {
	columns := []any{"id", "name", "sex", "age", "email", "addr", "registered_at", "logined_at", "group_id"}
	for i := 0; i < b.N; i++ {
		_distinct("", columns...)
	}
}

func TestClause_Count(t *testing.T) {
	sql, _ := _count("user")
	t.Log(string(sql))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_From-80    	20534457	        65.12 ns/op	      24 B/op	       2 allocs/op
func BenchmarkClause_Count(b *testing.B) {
	table := "user"
	for i := 0; i < b.N; i++ {
		_count(table)
	}
}

func TestClause_From(t *testing.T) {
	sqlSelect, _ := _select("", "name", "sex", "age")
	sqlDistinct, _ := _distinct("", "name", "age")
	sqlFrom, _ := _from("user")
	t.Log(string(sqlSelect) + string(sqlDistinct) + string(sqlFrom))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_From-80    	20534457	        65.12 ns/op	      24 B/op	       2 allocs/op
func BenchmarkClause_From(b *testing.B) {
	table := "user"
	for i := 0; i < b.N; i++ {
		_from(table)
	}
}

func TestClause_Where(t *testing.T) {
	sql, vars := _where("")
	t.Log(string(sql), vars)

	sql, vars = _where("",
		where.GT("a", 1),
		where.AND(where.LT("b", 3).OR(where.EQ("bb", "bbbb"))),
		where.IN("c", 1, 3, 5, 7),
		where.OR(where.BETWEEN("d", 1, 100).AND(where.NEQ("dd", "ddd", "ddd")).AND(where.NEQ("ddd", 6))),
		where.LIKE("e", "%123%"),
	)
	t.Log(string(sql), vars)

	sqlSelect, valSelect := _select("user", "name")
	sqlDistinct, valDistinct := _distinct("", "name", "sex", "age")
	sqlFrom, _ := _from("user")
	sqlWhere, valWhere := _where("", where.EQ("sex", "man"), where.OR(where.LT("age", 30, 10)), where.IN("name", "wx", "gh", "dsc"))
	t.Log(string(sqlSelect)+string(sqlDistinct)+string(sqlFrom)+string(sqlWhere), append(append(valSelect, valDistinct...), valWhere...))
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Where-80    	 1824906	       656.9 ns/op	     648 B/op	       9 allocs/op
func BenchmarkClause_Where(b *testing.B) {
	w1 := where.GT("a", 1)
	w2 := where.AND(where.LT("b", 3).OR(where.EQ("bb", "bbbb")))
	w3 := where.IN("c", 1, 3, 5, 7)
	w4 := where.OR(where.BETWEEN("d", 1, 100).AND(where.NEQ("dd", "ddd", "ddd")).AND(where.NEQ("ddd", 6)))
	w5 := where.LIKE("e", "%123%")
	for i := 0; i < b.N; i++ {
		_where("", w1, w2, w3, w4, w5)
	}
}

func TestClause_GroupBy(t *testing.T) {
	sql, vars := _groupBy("")
	t.Log(string(sql), vars)

	sql, vars = _groupBy("", "c")
	t.Log(string(sql), vars)

	sql, vars = _groupBy("", "c", nil, "d", "ddd", "a", "", "b")
	t.Log(string(sql), vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_GroupBy-80    	12232210	        96.12 ns/op	      48 B/op	       2 allocs/op
func BenchmarkClause_GroupBy(b *testing.B) {
	columns := []any{"c", nil, "d", "ddd", "a", "", "b"}
	for i := 0; i < b.N; i++ {
		_groupBy("", columns...)
	}
}

func TestClause_OrderBy(t *testing.T) {
	sql, vars := _orderBy("")
	t.Log(string(sql), vars)

	sql, vars = _orderBy("", "c")
	t.Log(string(sql), vars)

	sql, vars = _orderBy("", "d", "DESC")
	t.Log(string(sql), vars)

	sql, vars = _orderBy("", "a, b, c ASC")
	t.Log(string(sql), vars)

	sql, vars = _orderBy("", "c", "d", "desc", "ddd", "a ASC", "", "", "b desc")
	t.Log(string(sql), vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_OrderBy-80    	 6291066	       190.7 ns/op	     116 B/op	       4 allocs/op
func BenchmarkClause_OrderBy(b *testing.B) {
	columns := []any{"c", "d", "desc", "ddd", "a ASC", "", "", "b desc"}
	for i := 0; i < b.N; i++ {
		_orderBy("", columns...)
	}
}

func TestClause_Limit(t *testing.T) {
	sql, vars := _limit("", "a", 10, 10)
	t.Log(string(sql), vars)

	sql, vars = _limit("")
	t.Log(string(sql), vars)

	sql, vars = _limit("", 10)
	t.Log(string(sql), vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkClause_Limit-80    	11743837	       103.9 ns/op	      48 B/op	       2 allocs/op
func BenchmarkClause_Limit(b *testing.B) {
	columns := []any{"c", "a", 10, "b", 10, 10}
	for i := 0; i < b.N; i++ {
		_limit("", columns...)
	}
}
