package where

func AND(where *Where) *Where {
	return andOr("AND (", where)
}

func OR(where *Where) *Where {
	return andOr("OR (", where)
}

func andOr(cond string, where *Where, buf ...*Where) *Where {
	if len(buf) == 0 {
		if len(where.Dest) == 0 {
			return &Where{[]byte{}, []any{}}
		}

		dest := make([]byte, 0)
		dest = append(dest, cond...)
		dest = append(dest, where.Dest[:len(where.Dest)-1]...)
		dest = append(dest, ") "...)
		return &Where{dest, append([]any{}, where.Args...)}
	}

	if len(where.Dest) == 0 {
		return buf[0]
	}

	buf[0].Dest = append(buf[0].Dest, cond...)
	buf[0].Dest = append(buf[0].Dest, where.Dest[:len(where.Dest)-1]...)
	buf[0].Dest = append(buf[0].Dest, ") "...)
	buf[0].Args = append(buf[0].Args, where.Args...)
	return buf[0]
}

type Where struct {
	Dest []byte
	Args []any
}

func (own *Where) AND(where *Where) *Where {
	return own.andOr("AND ", where)
}

func (own *Where) OR(where *Where) *Where {
	return own.andOr("OR ", where)
}

func (own *Where) andOr(cond string, where *Where) *Where {
	if len(own.Dest) == 0 {
		if len(where.Dest) > 0 {
			own = where.Clone()
		}

		return own
	}

	if len(where.Dest) == 0 {
		return own
	}

	own.Dest = append(own.Dest, cond...)
	own.Dest = append(own.Dest, where.Dest...)
	own.Args = append(own.Args, where.Args...)
	return own
}

func (own *Where) IsEmpty() bool {
	return len(own.Dest) == 0
}

func (own *Where) Clone() *Where {
	dest := make([]byte, len(own.Dest))
	args := make([]any, len(own.Args))
	copy(dest, own.Dest)
	copy(args, own.Args)
	return &Where{dest, args}
}

func (own *Where) String() string {
	return string(own.Dest)
}
