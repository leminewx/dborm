package where

import "testing"

func TestWhere(t *testing.T) {
	where := &Where{Dest: []byte("aaaaa"), Args: []any{}}
	t.Log(where.Clone())
}

func TestAND(t *testing.T) {
	where := AND(GT("a", 1))
	t.Log(where, where.Args)

	where = AND(GTE("a", 1, 1))
	t.Log(where, where.Args)

	where = AND(LT("b", 2, 2))
	t.Log(where, where.Args)

	where = AND(LTE("b", 2))
	t.Log(where, where.Args)

	where = AND(EQ("c", "c"))
	t.Log(where, where.Args)

	where = AND(NEQ("c", "cc"))
	t.Log(where, where.Args)

	where = AND(IN("d", 1, 2, 3))
	t.Log(where, where.Args)

	where = AND(LIKE("e", "%e%"))
	t.Log(where, where.Args)

	where = AND(BETWEEN("f", 1, 100))
	t.Log(where, where.Args)
}

func TestOR(t *testing.T) {
	where := OR(GT("a", 1))
	t.Log(where, where.Args)

	where = OR(GTE("a", 1, 1))
	t.Log(where, where.Args)

	where = OR(LT("b", nil, nil))
	t.Log(where, where.Args)

	where = OR(LT("b", "", ""))
	t.Log(where, where.Args)

	where = OR(LTE("b", 2))
	t.Log(where, where.Args)

	where = OR(EQ("c", "c"))
	t.Log(where, where.Args)

	where = OR(NEQ("c", "cc"))
	t.Log(where, where.Args)

	where = OR(IN("d", 1, 2, 3))
	t.Log(where, where.Args)

	where = OR(LIKE("e", "%e%"))
	t.Log(where, where.Args)

	where = OR(BETWEEN("f", 1, 100))
	t.Log(where, where.Args)
}

func TestWhere_AND(t *testing.T) {
	where := GT("a", 1)
	t.Log(where, where.Args)

	where.AND(GTE("a", 1, 1))
	t.Log(where, where.Args)

	where.AND(LT("b", 2, 2))
	t.Log(where, where.Args)

	where.AND(LTE("b", 2))
	t.Log(where, where.Args)

	where.AND(EQ("c", "c"))
	t.Log(where, where.Args)

	where.AND(NEQ("c", "cc"))
	t.Log(where, where.Args)

	where.AND(IN("d", 1, 2, 3))
	t.Log(where, where.Args)

	where.AND(LIKE("e", "%e%"))
	t.Log(where, where.Args)

	where.AND(BETWEEN("f", 1, 100))
	t.Log(where, where.Args)
}

func TestWhere_OR(t *testing.T) {
	where := GT("a", 1)
	t.Log(where, where.Args)

	where.OR(GTE("a", 1, 1))
	t.Log(where, where.Args)

	where.OR(LT("b", 2, 2))
	t.Log(where, where.Args)

	where.OR(LTE("b", 2))
	t.Log(where, where.Args)

	where.OR(EQ("c", "c"))
	t.Log(where, where.Args)

	where.OR(NEQ("c", "cc"))
	t.Log(where, where.Args)

	where.OR(IN("d", 1, 2, 3))
	t.Log(where, where.Args)

	where.OR(LIKE("e", "%e%"))
	t.Log(where, where.Args)

	where.OR(BETWEEN("f", 1, 1))
	t.Log(where, where.Args)
}

func TestWhere_ANDmix(t *testing.T) {
	where := AND(GT("a", 1).AND(GTE("a", 1, 1)))
	t.Log(where, where.Args)

	where = AND(LT("b", 2).AND(LTE("b", 2)).OR(EQ("c", "c")))
	t.Log(where, where.Args)

	where = AND(NEQ("c", "", "").OR(IN("d", 1, 2, 3)).AND(LIKE("e", "%e%")).OR(BETWEEN("f", 1, 100)))
	t.Log(where, where.Args)
}

func TestWhere_ORmix(t *testing.T) {
	where := OR(GT("a", 1).OR(GTE("a", 1, 1)))
	t.Log(where, where.Args)

	where = OR(LT("b", 2).AND(LTE("b", 2)).OR(EQ("c", "c")))
	t.Log(where, where.Args)

	where = OR(NEQ("c", "", "").OR(IN("d", 1, 2, 3)).AND(LIKE("e", "%e%")).OR(BETWEEN("f", 1, 100)))
	t.Log(where, where.Args)
}

func TestWhere_ANDmixOR(t *testing.T) {
	where := OR(GT("a", 1).OR(GTE("a", 1, 1))).AND(EQ("c", "c"))
	t.Log(where, where.Args)

	where = AND(LT("b", 2)).OR(LTE("b", 2).OR(EQ("c", "c")))
	t.Log(where, where.Args)

	where = AND(NEQ("c", "cc").OR(IN("d", 1, 2, 3))).AND(LIKE("e", "%e%").OR(BETWEEN("f", 1, 100)))
	t.Log(where, where.Args)
}

// func TestOR(t *testing.T) {
// 	t.Log(OR(GT("a", 1)))
// 	t.Log(OR(GT("a", 1, 1)))
// 	t.Log(OR(LT("a", 1)))
// 	t.Log(OR(LT("a", 1, 1)))
// 	t.Log(OR(EQ("a", 1)))
// 	t.Log(OR(EQ("a", 1, 1)))
// 	t.Log(OR(IN("a", 1)))
// 	t.Log(OR(LIKE("a", "%%abc")))
// 	t.Log(OR(BETWEEN("a", 1, 100)))
// }
