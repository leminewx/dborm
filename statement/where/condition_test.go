package where

import "testing"

func TestWhere_EQ(t *testing.T) {
	t.Log(EQ("a", 123))
	t.Log(EQ("a", 123, 123))

	where := EQ("a", 1.2)
	t.Log(where, where.Args)
}

func TestWhere_NEQ(t *testing.T) {
	t.Log(NEQ("a", 123))
	t.Log(NEQ("a", 123, 123))

	where := NEQ("a", 1.2)
	t.Log(where, where.Args)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement/where
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkWhere_NEQ-80    	42335678	        28.37 ns/op	       8 B/op	       1 allocs/op
func BenchmarkWhere_NEQ(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NEQ("a", 1.2)
	}
}

func TestWhere_GT(t *testing.T) {
	t.Log(GT("a", 123))
	t.Log(GT("a", 123, 123))

	where := GT("a", 1.2)
	t.Log(where, where.Args)
}

func TestWhere_GTE(t *testing.T) {
	t.Log(GTE("a", 123))
	t.Log(GTE("a", 123, 123))

	where := GTE("a", 1.2)
	t.Log(where, where.Args)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement/where
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkWhere_NEQ-80    	42335678	        28.37 ns/op	       8 B/op	       1 allocs/op
func BenchmarkWhere_GTE(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GTE("a", 1.2)
	}
}

func TestWhere_LT(t *testing.T) {
	t.Log(LT("a", 123))
	t.Log(LT("a", 123, 123))

	where := LT("a", 1.2)
	t.Log(where, where.Args)
}

func TestWhere_LTE(t *testing.T) {
	t.Log(LTE("a", 123))
	t.Log(LTE("a", 123, 123))

	where := LTE("a", 1.2)
	t.Log(where, where.Args)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement/where
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkWhere_NEQ-80    	42335678	        28.37 ns/op	       8 B/op	       1 allocs/op
func BenchmarkWhere_LTE(b *testing.B) {
	for i := 0; i < b.N; i++ {
		LTE("a", 1.2)
	}
}

func TestWhere_LIKE(t *testing.T) {
	where := LIKE("a", "")
	t.Log(where, where.Args)

	where = LIKE("a", "a")
	t.Log(where, where.Args)

	where = LIKE("a", "%a")
	t.Log(where, where.Args)

	where = LIKE("a", "a%")
	t.Log(where, where.Args)

	where = LIKE("a", "%a%")
	t.Log(where, where.Args)

	where = LIKE("a", "a\\%")
	t.Log(where, where.Args)

	where = LIKE("a", "a\\%", true)
	t.Log(where, where.Args)
}

func TestWhere_FUZZ(t *testing.T) {
	where := FUZZ("a", "")
	t.Log(where, where.Args)

	where = FUZZ("a", "a")
	t.Log(where, where.Args)

	where = FUZZ("a", "a\\%")
	t.Log(where, where.Args)

	where = FUZZ("a", "a\\%", true)
	t.Log(where, where.Args)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement/where
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkWhere_LIKE-80    	45098908	        24.84 ns/op	       8 B/op	       1 allocs/op
func BenchmarkWhere_LIKE(b *testing.B) {
	for i := 0; i < b.N; i++ {
		LTE("a", 1.2)
	}
}

func TestWhere_IN(t *testing.T) {
	where := IN("a")
	t.Log(where, where.Args)

	where = IN("a", 1)
	t.Log(where, where.Args)

	where = IN("a", 1, 2)
	t.Log(where, where.Args)

	where = IN("a", "a")
	t.Log(where, where.Args)

	where = IN("a", "b", "c")
	t.Log(where, where.Args)
}

func TestWhere_NIN(t *testing.T) {
	where := NIN("a")
	t.Log(where, where.Args)

	where = NIN("a", 1)
	t.Log(where, where.Args)

	where = NIN("a", 1, 2)
	t.Log(where, where.Args)

	where = NIN("a", "a")
	t.Log(where, where.Args)

	where = NIN("a", "b", "c")
	t.Log(where, where.Args)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement/where
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkWhere_NIN-80    	 7635472	       155.7 ns/op	     104 B/op	       4 allocs/op
func BenchmarkWhere_NIN(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NIN("a", "b", "c")
	}
}

func TestWhere_BETWEEN(t *testing.T) {
	where := BETWEEN("a", nil, nil)
	t.Log(where, where.Args)

	where = BETWEEN("a", "b", nil)
	t.Log(where, where.Args)

	where = BETWEEN("a", nil, "c")
	t.Log(where, where.Args)

	where = BETWEEN("a", 1, 2)
	t.Log(where, where.Args)

	where = BETWEEN("a", "a", "k")
	t.Log(where, where.Args)
}

func TestWhere_NBETWEEN(t *testing.T) {
	where := NBETWEEN("a", nil, nil)
	t.Log(where, where.Args)

	where = NBETWEEN("a", 1, 2)
	t.Log(where, where.Args)

	where = NBETWEEN("a", 1, 100)
	t.Log(where, where.Args)

	where = NBETWEEN("a", "b", nil)
	t.Log(where, where.Args)

	where = NBETWEEN("a", "a", "b")
	t.Log(where, where.Args)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement/where
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkWhere_NBETWEEN-80    	19649326	        64.30 ns/op	      32 B/op	       2 allocs/op
func BenchmarkWhere_NBETWEEN(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NBETWEEN("a", 1, 100)
	}
}
