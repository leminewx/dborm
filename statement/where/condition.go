package where

const _SqlComma = ", "

// EQ 等于：column = value
func EQ(column string, value any, ignore ...any) *Where {
	return judgment(column, "=? ", value, ignore...)
}

// NEQ 不等于：column != value
func NEQ(column string, value any, ignore ...any) *Where {
	return judgment(column, "!=? ", value, ignore...)
}

// GT 大于：column > value
func GT(column string, value any, ignore ...any) *Where {
	return judgment(column, ">? ", value, ignore...)
}

// GTE 大于等于：column >= value
func GTE(column string, value any, ignore ...any) *Where {
	return judgment(column, ">=? ", value, ignore...)
}

// GTE 小于：column < value
func LT(column string, value any, ignore ...any) *Where {
	return judgment(column, "<? ", value, ignore...)

}

// GTE 小于等于：column <= value
func LTE(column string, value any, ignore ...any) *Where {
	return judgment(column, "<=? ", value, ignore...)
}

func judgment(column, condition string, value any, ignore ...any) *Where {
	dest := make([]byte, 0)
	if value == nil {
		return &Where{dest, []any{}}
	}

	// 如果 value == ignore，则忽略当前条件
	for _, i := range ignore {
		if i == value {
			return &Where{dest, []any{}}
		}
	}

	dest = append(dest, column...)
	dest = append(dest, condition...)
	return &Where{dest, []any{value}}

}

// LIKE 模糊匹配：LIKE "a"，LIKE "%a"，LIKE "a%"，LIKE "%a%"
func LIKE(column string, value string, escape ...bool) *Where {
	dest := make([]byte, 0)
	if value == "" {
		return &Where{dest, []any{}}
	}

	dest = append(dest, column...)
	if len(escape) > 0 && escape[0] {
		dest = append(dest, " LIKE ? ESCAPE '\\'"...)
	} else {
		dest = append(dest, " LIKE ? "...)
	}

	return &Where{dest, []any{value}}
}

// FUZZ 完全模糊匹配：LIKE "a" ===> LIKE "%a%"
func FUZZ(column string, value string, escape ...bool) *Where {
	dest := make([]byte, 0)
	if value == "" {
		return &Where{dest, []any{}}
	}

	dest = append(dest, column...)
	if len(escape) > 0 && escape[0] {
		dest = append(dest, " LIKE ? ESCAPE '\\'"...)
	} else {
		dest = append(dest, " LIKE ? "...)
	}

	return &Where{dest, []any{"%" + value + "%"}}
}

// IN 枚举匹配：IN (1, 2, 3, 4)
func IN(column string, values ...any) *Where {
	return in(column, " IN (", values)
}

// NIN 不在枚举范围中：NOT IN (1, 2, 3, 4)
func NIN(column string, values ...any) *Where {
	return in(column, " NOT IN (", values)
}

func in(column, condition string, values []any) *Where {
	dest := make([]byte, 0)
	if len(values) == 0 {
		return &Where{dest, []any{}}
	}

	dest = append(dest, column...)
	dest = append(dest, condition...)
	dest = bindVariables(dest, len(values))
	dest = append(dest, ") "...)
	return &Where{dest, values}
}

// BETWEEN 范围匹配：BETWEEN 1 AND 100
func BETWEEN(column string, from, to any) *Where {
	return between(column, " BETWEEN ? AND ? ", from, to)
}

// NBETWEEN 不在匹配范围中：NOT BETWEEN 1 AND 100
func NBETWEEN(column string, from, to any) *Where {
	return between(column, " NOT BETWEEN ? AND ? ", from, to)
}

func between(column, condition string, from, to any) *Where {
	dest := make([]byte, 0)
	if from == nil || to == nil {
		return &Where{dest, []any{}}
	}

	dest = append(dest, column...)
	dest = append(dest, condition...)
	return &Where{dest, []any{from, to}}
}

func bindVariables(buf []byte, num int) []byte {
	for i := 0; i < num; i++ {
		if i > 0 {
			buf = append(buf, _SqlComma...)
		}

		buf = append(buf, '?')
	}

	return buf
}
