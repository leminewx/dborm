package statement

import (
	"testing"

	"gitee.com/leminewx/dborm/statement/where"
)

func TestStatement_Select(t *testing.T) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseSelect, "name", "sex").
		WithClause(ClauseWhere, where.GT("a", 1), where.AND(where.LT("b", 3, 4).OR(where.IN("c", 1, 3, 5, 7))), where.OR(where.BETWEEN("d", 1, 100)), where.LIKE("e", "%123%")).
		WithClause(ClauseGroupBy, "sex", "age").
		WithClause(ClauseOrderBy, "sex", "desc", "age", "addr", "email ASC").
		WithClause(ClauseLimit, 3, 100)

	sql, vars := statement.GenerateSQL(ClauseSelect, ClauseFrom, ClauseWhere, ClauseGroupBy, ClauseOrderBy, ClauseLimit)
	t.Log(sql, vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkStatement_Select-80    	 1711257	       697.8 ns/op	     984 B/op	       7 allocs/op
func BenchmarkStatement_Select(b *testing.B) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseSelect, "name", "sex").
		WithClause(ClauseWhere, where.GT("a", 1), where.AND(where.LT("b", 3, 4).OR(where.IN("c", 1, 3, 5, 7))), where.OR(where.BETWEEN("d", 1, 100)), where.LIKE("e", "%123%")).
		WithClause(ClauseGroupBy, "sex", "age").
		WithClause(ClauseOrderBy, "sex", "desc", "age", "addr", "asc", "email").
		WithClause(ClauseLimit, 3, 100)

	for i := 0; i < b.N; i++ {
		statement.GenerateSQL(ClauseSelect, ClauseFrom, ClauseWhere, ClauseGroupBy, ClauseOrderBy, ClauseLimit)
	}
}

func TestStatement_Insert(t *testing.T) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseInsert, "name", "age", "sex", "emial", "addr", "group_id").
		WithClause(ClauseValues,
			[]any{"lemine", 18, true, "xxx@qq.com", "chongqing", 3},
			[]any{"lemon", 18, false, "xxx@qq.com", "wuhang", 2},
		)

	sql, vars := statement.GenerateSQL(ClauseInsert, ClauseValues)
	t.Log(sql, vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkStatement_Insert-80    	 4070233	       293.6 ns/op	     386 B/op	       5 allocs/op
func BenchmarkStatement_Insert(b *testing.B) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseInsert, "name", "age", "sex", "emial", "addr", "group_id").
		WithClause(ClauseValues, []any{"lemine", 18, true, "xxx@qq.com", "chongqing", 3})

	for i := 0; i < b.N; i++ {
		statement.GenerateSQL(ClauseInsert, ClauseValues)
	}
}

func TestStatement_Update(t *testing.T) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseUpdate, "age", 30, "email", "xxxx@qq.com").
		WithClause(ClauseWhere, where.EQ("name", "wx", ""), where.OR(where.EQ("sex", "man", "").AND(where.IN("goroup_id", 1, 2, 3, 4))))

	sql, vars := statement.GenerateSQL(ClauseUpdate, ClauseWhere)
	t.Log(sql, vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkStatement_Insert-80    	 4070233	       293.6 ns/op	     386 B/op	       5 allocs/op
func BenchmarkStatement_Update(b *testing.B) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseUpdate, "age", 30, "email", "xxxx@qq.com").
		WithClause(ClauseWhere, where.EQ("name", "wx", ""), where.OR(where.EQ("sex", "man", "").AND(where.IN("goroup_id", 1, 2, 3, 4))))

	for i := 0; i < b.N; i++ {
		statement.GenerateSQL(ClauseUpdate, ClauseWhere)
	}
}

func TestStatement_Delete(t *testing.T) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseDelete).
		WithClause(ClauseWhere, where.EQ("name", "wx", "").OR(where.LT("age", 18)), where.IN("addr", "beijing", "chongqing", "sicuan"))

	sql, vars := statement.GenerateSQL(ClauseDelete, ClauseWhere)
	t.Log(sql, vars)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/statement
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkStatement_Delete-80    	 4683112	       251.9 ns/op	     234 B/op	       5 allocs/op
func BenchmarkStatement_Delete(b *testing.B) {
	statement := NewStatement("user")
	statement.
		WithClause(ClauseDelete).
		WithClause(ClauseWhere, where.EQ("name", "wx", "").OR(where.LT("age", 18)), where.IN("addr", "beijing", "chongqing", "sicuan"))

	for i := 0; i < b.N; i++ {
		statement.GenerateSQL(ClauseDelete, ClauseWhere)
	}
}
