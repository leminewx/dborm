package session

import (
	"database/sql"
	"testing"

	"gitee.com/leminewx/dborm/dialect"
	"gitee.com/leminewx/dborm/statement/where"
)

func TestSession_Insert(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()

	// session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestGroup{})
	// if _, err := session.Insert(
	// 	&TestGroup{ID: 1, Name: "A"},
	// 	&TestGroup{ID: 1, Name: "B"},
	// 	&TestGroup{ID: 1, Name: "C"},
	// 	&TestGroup{ID: 1, Name: "D"},
	// ); err != nil {
	// 	t.Fatal(err)
	// }

	session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestUser{})
	if _, err := session.Insert(
		&TestUser{ID: 1, Name: "wx", Age: 18, Sex: "boy", Addr: "chongqing", Email: "wx@qq.com", GroupId: 1},
		&TestUser{ID: 1, Name: "gh", Age: 19, Sex: "boy", Addr: "suzhou", Email: "gh@qq.com", GroupId: 2},
		&TestUser{ID: 1, Name: "dsc", Age: 20, Sex: "boy", Addr: "beijing", Email: "dsc@qq.com", GroupId: 3},
		&TestUser{ID: 1, Name: "zjh", Age: 21, Sex: "boy", Addr: "beijing", Email: "zjh@qq.com", GroupId: 4},
	); err != nil {
		t.Fatal(err)
	}
}

func TestSession_Update(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()

	session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestUser{})
	if _, err := session.Where(where.EQ("name", "wx")).Update(map[string]any{"email": "wx@163.com"}); err != nil {
		t.Fatal(err)
	}
}

func TestSession_Select(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()

	session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestUser{})

	var user TestUser
	if err := session.Where().Select(&user); err != nil {
		t.Fatal(err)
	}
	t.Log(user)

	var users []TestUser
	if err := session.Reset().Where().Select(&users); err != nil {
		t.Fatal(err)
	}
	t.Log(users)

	users = []TestUser{}
	if err := session.Reset().Where(where.EQ("name", "wx")).Select(&users); err != nil {
		t.Fatal(err)
	}
	t.Log(users)
}

// goos: linux
// goarch: amd64
// pkg: gitee.com/leminewx/dborm/session
// cpu: QEMU Virtual CPU version 2.5+
// BenchmarkSession_Select-80    	    3046	    390708 ns/op	    3975 B/op	      77 allocs/op
func BenchmarkSession_Select(b *testing.B) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()

	session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestUser{}).WithoutPrint()
	session.Where(where.EQ("name", "wx").AND(where.IN("group_id", 1, 2, 3, 4)))
	for i := 0; i < b.N; i++ {
		var users []TestUser
		if err := session.Select(&users); err != nil {
			b.Fatal(err)
		}
	}
}

func TestSession_Count(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()

	session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestUser{})
	num, err := session.Where().Count()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(num)
}

// func TestSession_Select(t *testing.T) {
// 	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
// 	defer TestDB.Close()
// 	testDial, _ := dialect.Get("mysql")

// 	var users []TestUser
// 	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{})
// 	sess := session.Where(where.LT("age", 25), where.IN("name", "wx", "gh", "cj", "dsc"))
// 	count, err := sess.Count()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Log("count:", count)

// 	if err := sess.Limit(1, 2).Select(&users); err != nil {
// 		t.Fatal(err)
// 	}
// 	for _, user := range users {
// 		t.Log(user)
// 	}
// }

// func TestSession_SelectFirst(t *testing.T) {
// 	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
// 	defer TestDB.Close()
// 	testDial, _ := dialect.Get("mysql")

// 	var user TestUser
// 	session := New("test", TestDB, testDial, nil).SetModel(&TestUser{}).Where(where.GT("age", 25), where.IN("name", "zp", "wx", "gh")).Limit(5)
// 	if err := session.SelectFirst(&user); err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Log(user)
// }

// func TestSession_Limit(t *testing.T) {
// 	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
// 	defer TestDB.Close()
// 	testDial, _ := dialect.Get("mysql")

// 	session := New("test", TestDB, testDial, nil)
// 	var users []TestUser
// 	if err := session.SetModel(&TestUser{}).Limit(1).Select(&users); err != nil {
// 		t.Fatal(err)
// 	} else if len(users) != 1 {
// 		t.Fatal("failed to query with limit condition")
// 	}

// 	t.Log(users)
// }

// func TestSession_OrderBy(t *testing.T) {
// 	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
// 	defer TestDB.Close()
// 	testDial, _ := dialect.Get("mysql")

// 	session := New("test", TestDB, testDial, nil)
// 	var users []TestUser
// 	if err := session.SetModel(&TestUser{}).OrderBy("sex", "asc").Select(&users); err != nil {
// 		t.Fatal(err)
// 	}
// 	for _, user := range users {
// 		t.Log(user)
// 	}

// 	users = []TestUser{}
// 	if err := session.SetModel(&TestUser{}).OrderBy("sex", "desc", "age").Select(&users); err != nil {
// 		t.Fatal(err)
// 	}
// 	for _, user := range users {
// 		t.Log(user)
// 	}
// }

// func TestSession_Update(t *testing.T) {
// 	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
// 	defer TestDB.Close()
// 	testDial, _ := dialect.Get("mysql")

// 	session := New("test", TestDB, testDial, nil)
// 	affected, err := session.SetModel(&TestUser{}).Where(where.EQ("name", "wx")).Update("Age", 31)
// 	if err != nil {
// 		t.Fatal(err)
// 	}

// 	var user TestUser
// 	if err := session.SelectFirst(&user); err != nil {
// 		t.Fatal(err)
// 	}

// 	if affected != 1 {
// 		t.Fatal("failed to update")
// 	}

// 	t.Log(user)
// }

// func TestSession_DeleteAndCount(t *testing.T) {
// 	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
// 	defer TestDB.Close()
// 	testDial, _ := dialect.Get("mysql")

// 	session := New("test", TestDB, testDial, nil)
// 	affected, err := session.SetModel(&TestUser{}).Where(where.EQ("name", "gh")).Delete()
// 	if err != nil {
// 		t.Fatal(err)
// 	}

// 	if affected != 1 {
// 		t.Fatal("failed to delete or count")
// 	}
// }
