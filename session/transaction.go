package session

import (
	"fmt"

	"gitee.com/leminewx/loggo"
)

// Begin 开始事务
func (own *Session) Begin() (err error) {
	loggo.Info("dborm: begin transaction.")
	own.tx, err = own.db.Begin()
	return err
}

// Commit 提交事务
func (own *Session) Commit() error {
	if own.tx == nil {
		return fmt.Errorf("dborm: transaction is not started yet")
	}

	loggo.Info("dborm: commit transaction.")
	return own.tx.Commit()
}

// Rollback 回滚事务
func (own *Session) Rollback() error {
	if own.tx == nil {
		return fmt.Errorf("dborm: transaction id not started yet")
	}

	loggo.Info("dborm: rollback transaction.")
	return own.tx.Rollback()
}
