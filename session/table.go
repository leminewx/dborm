package session

import (
	"fmt"
	"reflect"

	"gitee.com/leminewx/dborm/schema"
)

// WithModel 指定数据表模型
func (own *Session) WithModel(model any) *Session {
	if own.schema == nil || reflect.TypeOf(model) != own.schema.ModelType {
		own.schema = schema.Parse(own.dialect, model)
		own.statement.WithTableName(own.schema.Name)
	}

	return own
}

// GetSchema 获取数据表信息
func (own *Session) GetSchema() (*schema.Schema, error) {
	if own.schema == nil {
		return nil, fmt.Errorf("dborm: no data model specified")
	}

	if len(own.schema.Columns) == 0 {
		return nil, fmt.Errorf("dborm: no exportable fields in data model")
	}

	return own.schema, nil
}

// CreateTable 创建数据表
func (own *Session) CreateTable(engine ...string) error {
	schema, err := own.GetSchema()
	if err != nil {
		return err
	}

	sql := own.dialect.GenerateCreatingTableSQL(schema.Name, schema.Columns, engine...)
	_, err = own.Exec(sql)
	return err
}

// DropTable 删除数据表
func (own *Session) DropTable() error {
	schema, err := own.GetSchema()
	if err != nil {
		return err
	}

	_, err = own.Exec("DROP TABLE IF EXISTS " + schema.Name + ";")
	return err
}

// IsExistTable 检查数据表是否存在
func (own *Session) IsExistTable() bool {
	schema, err := own.GetSchema()
	if err != nil {
		return false
	}

	sql := own.dialect.GenerateQueryingTableSQL(own.dbName, schema.Name)
	row := own.QueryRow(sql)

	var tmp string
	_ = row.Scan(&tmp)
	return tmp == schema.Name
}
