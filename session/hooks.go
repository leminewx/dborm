package session

import (
	"fmt"
	"reflect"
)

// Hooks constants
const (
	HOOK_AFTER_SELECT  = "CallAfterSelect"
	HOOK_AFTER_UPDATE  = "CallAfterUpdate"
	HOOK_AFTER_DELETE  = "CallAfterDelete"
	HOOK_AFTER_INSERT  = "CallAfterInsert"
	HOOK_BEFORE_SELECT = "CallBeforeSelect"
	HOOK_BEFORE_UPDATE = "CallBeforeUpdate"
	HOOK_BEFORE_DELETE = "CallBeforeDelete"
	HOOK_BEFORE_INSERT = "CallBeforeInsert"
)

// CallMethod calls the registered hooks
func (own *Session) CallMethod(method string, value any) error {
	// 获取数据表
	schema, err := own.GetSchema()
	if err != nil {
		return err
	}

	// 获取数据表的指定方法
	fn := reflect.ValueOf(schema.Model).MethodByName(method)
	if value != nil {
		fn = reflect.ValueOf(value).MethodByName(method)
	}

	if !fn.IsValid() {
		return fmt.Errorf("dborm: not found method: %s", method)
	}

	param := []reflect.Value{reflect.ValueOf(own)}

	// 循环调用
	if v := fn.Call(param); len(v) > 0 {
		if err, ok := v[0].Interface().(error); ok {
			return err
		}
	}

	return nil
}
