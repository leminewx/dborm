package session

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"

	"gitee.com/leminewx/dborm/schema"
	"gitee.com/leminewx/dborm/statement"
	"gitee.com/leminewx/dborm/statement/where"
)

// Insert 插入一条或多条数据到数据表
func (own *Session) Insert(values ...any) (sql.Result, error) {
	if len(values) == 0 {
		return nil, fmt.Errorf("insert empty values")
	}

	schema, err := own.GetSchema()
	if err != nil {
		return nil, err
	}

	// 循环调用 CallBeforeInsert 方法，同时提取数据
	recordValues := make([]any, 0, len(values))
	for _, value := range values {
		own.CallMethod(HOOK_BEFORE_INSERT, value)
		recordValues = append(recordValues, schema.ExtractValues(value))
	}

	// 添加 INSERT 子句和 VALUES 子句，然后生成完整的 SQL 语句，并执行
	own.statement.WithClause(statement.ClauseInsert, schema.InsertableNames...)
	own.statement.WithClause(statement.ClauseValues, recordValues...)
	sql, args := own.statement.GenerateSQL(statement.ClauseInsert, statement.ClauseValues)
	result, err := own.Exec(sql, args...)
	if err != nil {
		return nil, err
	}

	// 调用 CallAfterInsert
	own.CallMethod(HOOK_AFTER_INSERT, nil)
	return result, nil
}

// Update 根据条件更新数据，支持 map[string]any 或键值对格式的列表，如："Name"，"Tom"，"Age"，18，....
func (own *Session) Update(values ...any) (int64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("update empty values")
	}

	// 调用 CallBeforeUpdate 方法
	own.CallMethod(HOOK_BEFORE_UPDATE, nil)

	// 添加 INSERT 子句，然后结合 WHERE 子句生成完整的 SQL 语句，并执行
	own.statement.WithClause(statement.ClauseUpdate, values...)
	sql, args := own.statement.GenerateSQL(statement.ClauseUpdate, statement.ClauseWhere)
	result, err := own.Exec(sql, args...)
	if err != nil {
		return 0, err
	}

	// 调用 CallAfterUpdate 方法
	own.CallMethod(HOOK_AFTER_UPDATE, nil)
	return result.RowsAffected()
}

// Delete 根据条件删除数据
func (own *Session) Delete() (int64, error) {
	// 调用 CallBeforeDelete 方法
	own.CallMethod(HOOK_BEFORE_DELETE, nil)

	// 添加 DELETE 子句，然后结合 WHERE 子句生成完整的 SQL 语句，并执行
	own.statement.WithClause(statement.ClauseDelete)
	sql, args := own.statement.GenerateSQL(statement.ClauseDelete, statement.ClauseWhere)
	result, err := own.Exec(sql, args...)
	if err != nil {
		return 0, err
	}

	// 调用 CallAfterDelete 方法
	own.CallMethod(HOOK_AFTER_DELETE, nil)
	return result.RowsAffected()
}

// Select 根据条件查询第一个或所有数据
func (own *Session) Select(values any) error {
	schema, err := own.GetSchema()
	if err != nil {
		return err
	}

	// 调用 CallBeforeSelect 方法
	own.CallMethod(HOOK_BEFORE_SELECT, nil)

	// 解析数据模型
	dest := reflect.Indirect(reflect.ValueOf(values))
	destKind := dest.Kind()

	var onlyOne bool
	var destType reflect.Type
	if destKind == reflect.Slice { // 多值
		destType = dest.Type().Elem()
		if destType.Kind() == reflect.Ptr {
			destType = destType.Elem()
		}
	} else if destKind == reflect.Ptr || destKind == reflect.Struct { // 单值
		onlyOne = true
		if destKind == reflect.Ptr {
			destType = dest.Type().Elem()
		} else {
			destType = dest.Type()
		}
	}

	if destType != schema.ModelType {
		return fmt.Errorf("inconsistent data model")
	}

	if onlyOne {
		return own.selectFirst(schema, dest, destType)
	}

	return own.selectMore(schema, dest, destType)
}

func (own *Session) selectFirst(schema *schema.Schema, dest reflect.Value, destType reflect.Type) error {
	destMore := reflect.New(reflect.SliceOf(dest.Type())).Elem()
	if err := own.Limit(1).selectMore(schema, destMore, destType); err != nil {
		return err
	} else if destMore.Len() == 0 {
		return errors.New("not found data")
	}

	dest.Set(destMore.Index(0))
	return nil
}

func (own *Session) selectMore(schema *schema.Schema, dest reflect.Value, destType reflect.Type) error {
	// 添加 SELECT 子句，然后结合 WHERE 子句生成完整的 SQL 语句，并执行
	own.statement.WithClause(statement.ClauseSelect, schema.ColumnNames...)
	sql, args := own.statement.GenerateSQL(statement.ClauseSelect, statement.ClauseFrom, statement.ClauseWhere, statement.ClauseGroupBy, statement.ClauseHaving, statement.ClauseOrderBy, statement.ClauseLimit)
	rows, err := own.Query(sql, args...)
	if err != nil {
		return err
	}

	// 从返回值中提取、封装数据
	for rows.Next() {
		var values []any
		value := reflect.New(destType).Elem()
		for _, column := range schema.Columns {
			values = append(values, value.FieldByName(column.GetName()[0]).Addr().Interface())
		}

		if err := rows.Scan(values...); err != nil {
			return err
		}

		// 调用 CallAfterSelect 方法
		own.CallMethod(HOOK_AFTER_SELECT, value.Addr().Interface())
		dest.Set(reflect.Append(dest, value))
	}

	if err := rows.Err(); err != nil {
		return err
	}

	return rows.Close()
}

// Count 根据条件统计数据
func (own *Session) Count() (int64, error) {
	own.statement.WithClause(statement.ClauseCount)
	sql, args := own.statement.GenerateSQL(statement.ClauseCount, statement.ClauseWhere)
	row := own.QueryRow(sql, args...)

	var count int64
	err := row.Scan(&count)
	return count, err
}

// Where 添加 WHERE 子句
func (own *Session) Where(conds ...*where.Where) *Session {
	if len(conds) == 0 {
		return own
	}

	_conds := ConvertToAny(conds...)
	own.statement.WithClause(statement.ClauseWhere, _conds...)
	return own
}

// GroupBy 添加 GROUP BY 子句
func (own *Session) GroupBy(columns ...string) *Session {
	if len(columns) == 0 {
		return own
	}

	_columns := ConvertToAny(columns...)
	own.statement.WithClause(statement.ClauseGroupBy, _columns...)
	return own
}

// OrderBy 添加 ORDER BY 子句
func (own *Session) OrderBy(orders ...string) *Session {
	if len(orders) == 0 {
		return own
	}

	_orders := ConvertToAny(orders...)
	own.statement.WithClause(statement.ClauseOrderBy, _orders...)
	return own
}

// Limit 添加 LIMIT 子句
func (own *Session) Limit(limits ...int) *Session {
	if len(limits) == 0 {
		return own
	}

	_limits := ConvertToAny(limits...)
	own.statement.WithClause(statement.ClauseLimit, _limits...)
	return own
}

func ConvertToAny[T any](vals ...T) []any {
	_vals := make([]any, 0, len(vals))
	for _, cond := range vals {
		_vals = append(_vals, cond)
	}

	return _vals
}
