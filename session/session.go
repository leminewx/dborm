package session

import (
	"database/sql"
	"fmt"

	"gitee.com/leminewx/dborm/dialect"
	"gitee.com/leminewx/dborm/schema"
	"gitee.com/leminewx/dborm/statement"
	"gitee.com/leminewx/loggo"
	"gitee.com/leminewx/loggo/attr"
)

// Session 定义数据库连接会话的数据结构
type Session struct {
	print     bool
	dbName    string
	db        *sql.DB
	tx        *sql.Tx
	schema    *schema.Schema
	dialect   dialect.Dialect
	statement *statement.Statement
}

// New 新建一个连接会话
func New(dbName string, db *sql.DB, dial dialect.Dialect, schema *schema.Schema) *Session {
	sess := &Session{
		print:     true,
		dbName:    dbName,
		db:        db,
		dialect:   dial,
		schema:    schema,
		statement: statement.NewStatement(""),
	}

	if schema != nil {
		sess.statement.WithTableName(schema.Name)
	}

	return sess
}

func (own *Session) WithoutPrint() *Session {
	own.print = false
	return own
}

// Reset 重置 SQL 语句
func (own *Session) Reset() *Session {
	own.statement.Reset()
	return own
}

// BaseDB 定义数据库操作的基本接口
type BaseDB interface {
	Exec(query string, args ...any) (sql.Result, error)
	Query(query string, args ...any) (*sql.Rows, error)
	QueryRow(query string, args ...any) *sql.Row
}

var (
	_ BaseDB = (*sql.DB)(nil)
	_ BaseDB = (*sql.Tx)(nil)
)

// GetDB 返回 *sql.DB 或 *sql.TX
func (own *Session) GetDB() BaseDB {
	if own.tx != nil {
		return own.tx
	}

	return own.db
}

// Exec 执行 SQL 语句
func (own *Session) Exec(query string, args ...any) (sql.Result, error) {
	if own.print {
		if len(args) == 0 {
			// \033[32m%s\033[0m
			// \033[0;32;40m%s\033[0m
			loggo.Info(fmt.Sprintf("\033[32m%s\033[0m", query))
		} else {
			loggo.Info(fmt.Sprintf("\033[32m%s\033[0m", query), attr.New("args", args))
		}
	}

	return own.GetDB().Exec(query, args...)
}

// Query 执行 SQL 查询语句，返回多条结果
func (own *Session) Query(query string, args ...any) (*sql.Rows, error) {
	if own.print {
		if len(args) == 0 {
			loggo.Info(fmt.Sprintf("\033[32m%s\033[0m", query))
		} else {
			loggo.Info(fmt.Sprintf("\033[32m%s\033[0m", query), attr.New("args", args))
		}
	}

	return own.GetDB().Query(query, args...)
}

// QueryRow 执行 SQL 查询语句，返回单条结果
func (own *Session) QueryRow(query string, args ...any) *sql.Row {
	if own.print {
		if len(args) == 0 {
			loggo.Info(fmt.Sprintf("\033[32m%s\033[0m", query))
		} else {
			loggo.Info(fmt.Sprintf("\033[32m%s\033[0m", query), attr.New("args", args))
		}
	}

	return own.GetDB().QueryRow(query, args...)
}
