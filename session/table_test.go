package session

import (
	"database/sql"
	"testing"
	"time"

	"gitee.com/leminewx/dborm/dialect"
)

type TestGroup struct {
	ID   int    `dborm:"'id' pk autoinc"`
	Name string `dborm:"unique"`
}

func (own *TestGroup) GetTableName() string {
	return "test_groups"
}

type TestUser struct {
	Ignore     int64     `dborm:"-"`
	ID         int64     `dborm:"'id' pk autoinc comment('用户ID')"`
	Name       string    `dborm:"varchar(30) notnull index(idt) comment('用户名')"`
	Age        uint8     `dborm:"tinyint(1) default(0) comment('年龄')"`
	Sex        string    `dborm:"enum('boy','gril') notnull comment('0-男；1-女')"`
	Addr       string    `dborm:"default('') comment('地址')"`
	Email      string    `dborm:"unique index(idt) default('') comment('邮箱')"`
	RegistedAt time.Time `dborm:"created comment('注册时间')"`
	LoginedAt  time.Time `dborm:"timestamp updated comment('登录时间')"`
	GroupId    int       `dborm:"fk(test_groups(id)) comment('所属组ID')"`
	Extras     []byte
}

func (own *TestUser) GetTableName() string {
	return "test_users"
}

func TestSession_WithModel(t *testing.T) {
	sess := New("test", nil, dialect.Get(dialect.Mysql), nil)
	sess.WithModel(&TestUser{})
	t.Log(sess.schema.Name, sess.schema.ModelType)

	sess.WithModel(TestUser{})
	t.Log(sess.schema.Name, sess.schema.ModelType)

	sess.WithModel(&TestUser{})
	t.Log(sess.schema.Name, sess.schema.ModelType)

	t.Log(sess.dialect.GenerateCreatingTableSQL(sess.schema.Name, sess.schema.Columns))
}

func TestSession_GetSchema(t *testing.T) {
	sess := New("test", nil, dialect.Get(dialect.Mysql), nil)
	t.Log(sess.GetSchema())

	sess.WithModel(&TestUser{})
	t.Log(sess.GetSchema())

	sess.WithModel(TestUser{})
	t.Log(sess.GetSchema())

	sess.WithModel(&TestUser{})
	t.Log(sess.GetSchema())

	t.Log(sess.dialect.GenerateCreatingTableSQL(sess.schema.Name, sess.schema.Columns))
}

func TestSession_CreateTable(t *testing.T) {
	TestDB, _ := sql.Open("mysql", "root:mysql#123@tcp(10.254.249.16:3306)/test?parseTime=true&loc=Local")
	defer TestDB.Close()

	session := New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestGroup{})
	session.DropTable()
	if err := session.CreateTable(); err != nil {
		t.Fatal(err)
	}
	if !session.IsExistTable() {
		t.Fatal("Failed to create table test_groups")
	}

	session = New("test", TestDB, dialect.Get("mysql"), nil).WithModel(&TestUser{})
	session.DropTable()
	if err := session.CreateTable(); err != nil {
		t.Fatal(err)
	}

	if !session.IsExistTable() {
		t.Fatal("Failed to create table test_users")
	}
}
